"use strict";
if (typeof (Shopify) == "undefined") {
    var domain_url = "http://localhost/private-apps/hl-b2b/hl-b2b.php";
} else {
    var shop = Shopify.shop;
    var domain_url = "https://codelocksolutions.com//getreview/getreview.php";
    console.log(domain_url);
}



// var link = document.createElement('link');
// link.href = 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css';
// link.rel = 'stylesheet';
// document.getElementsByTagName('head')[0].appendChild(link);
(function() {
    // Load the script
    var script = document.createElement("SCRIPT");
    script.src = 'https://code.jquery.com/jquery-3.6.0.min.js';
    script.type = 'text/javascript';
    function loadjscssfile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}
loadjscssfile("https://codelocksolutions.com/getreview/assets/css/front_css", "css")

    script.onload = function() {
        var $ = window.jQuery;
        var front_url = window.location.pathname.split("/");
        console.log(front_url);
        console.log(window.location.pathname + "url");
        console.log(shop + "shop");
        if(shop == 'personalbooks4kids.myshopify.com'){
            console.log("in store");
            if (window.location.href.indexOf("/products") > -1) {
            console.log("in products");
            $.ajax({
            url: domain_url,
            type: "post",
            dataType: "json",
            data: {'routine_name':'prepare_html','store' : shop},
            beforeSend: function () {
                
            },
            success: function (response) {
                console.log(response);
               $(".product-single").append(response['html']);
            }
        });
        }
        }else{
             if (window.location.href.indexOf("/products") > -1) {
            console.log("in products");
            $.ajax({
            url: domain_url,
            type: "post",
            dataType: "json",
            data: {'routine_name':'prepare_html','store' : shop},
            beforeSend: function () {
                
            },
            success: function (response) {
               $("#wrap_des_pr").append(response['html']);
            }
        });
        }
        }
        
        
    
    };
    document.getElementsByTagName("head")[0].appendChild(script);
})();

// var s = document.createElement("script"); 
// s.src = "https://code.jquery.com/jquery-3.6.0.min.js"; 
// s.onload = function(e){ /* now that its loaded, do something */ };  
// document.head.appendChild(s);  


/****************************
 *  SOME COMMON SVG CONSTANT *
 ****************************/
var SVG_LOADER = '<svg viewBox="0 0 20 20" class="Polaris-Spinner Polaris-Spinner--colorInkLightest Polaris-Spinner--sizeSmall" aria-label="Loading" role="status"><path d="M7.229 1.173a9.25 9.25 0 1 0 11.655 11.412 1.25 1.25 0 1 0-2.4-.698 6.75 6.75 0 1 1-8.506-8.329 1.25 1.25 0 1 0-.75-2.385z"></path></svg>';
var SVG_DELETE = '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg>';

var SVG_MINUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 80 80" focusable="false" aria-hidden="true"><path d="M39.769,0C17.8,0,0,17.8,0,39.768c0,21.956,17.8,39.768,39.769,39.768   c21.965,0,39.768-17.812,39.768-39.768C79.536,17.8,61.733,0,39.769,0z M13.261,45.07V34.466h53.014V45.07H13.261z" fill-rule="evenodd" fill="#DE3618"></path></svg>';
var SVG_PLUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 9h-6V3a1 1 0 1 0-2 0v6H3a1 1 0 1 0 0 2h6v6a1 1 0 1 0 2 0v-6h6a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg>';

var SVG_CIRCLE_MINUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 55 55" focusable="false" aria-hidden="true"><path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M38.5,28h-25c-1.104,0-2-0.896-2-2  s0.896-2,2-2h25c1.104,0,2,0.896,2,2S39.604,28,38.5,28z" fill-rule="evenodd" fill="#DE3618"></path></svg>';
var SVG_CIRCLE_PLUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 510 510" focusable="false" aria-hidden="true"><path d="M255,0C114.75,0,0,114.75,0,255s114.75,255,255,255s255-114.75,255-255S395.25,0,255,0z M382.5,280.5h-102v102h-51v-102    h-102v-51h102v-102h51v102h102V280.5z" fill-rule="evenodd" fill="#3f4eae"></path></svg>';

//alert(shop);
/****************************************
 * For Tab end
 ****************************************/
function loading_show($selector) {
    $($selector).addClass("Polaris-Button--loading").html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner">' + SVG_LOADER + '</span><span>Loading</span></span>').fadeIn('fast').attr('disabled', 'disabled');
}

/**
 * @param {string} $className
 * @param {string} $buttonName
 * @returns {undefined} hide loader
 */
function loading_hide_n_show_html($selector, $html) {
    $($selector).removeClass("Polaris-Button--loading").html($html).removeAttr("disabled");
}
function loading_hide($selector, $buttonName, $buttonIcon) {

    if ($buttonIcon != undefined) {
        $buttonIcon = '<span class="Polaris-Button__Icon"><span class="Polaris-Icon">' + $buttonIcon + '</span></span>'
    } else {
        $buttonIcon = '';
    }

    $($selector).removeClass("Polaris-Button--loading").html('<span class="Polaris-Button__Content">' + $buttonIcon + '<span>' + $buttonName + '</span></span>').removeAttr("disabled");
}

/**
 * loader for table 
 * param 1 for selector of table it either id,class or anyting else
 * param 2 generally it is the number of column which our table have
 */
function table_loader(selector, colSpan) {
    $(selector).html('<tr><td colspan="' + colSpan + '" style="text-align:center;"><img src="https://cdn.shopify.com/s/files/1/0076/0741/8944/t/2/assets/loadding-page.gif"></div></div></td></tr>')
}

function changeTab(evt, id) {

    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("Polaris-Tabs_tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("Polaris-Tabs__Tab");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace("Polaris-Tabs__Tab--selected", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(id).style.display = "block";
    evt.currentTarget.className += " Polaris-Tabs__Tab--selected";
}


var loadApiListDataAjax = null;
var __loadApiListData = function __loadApiListData(tableId, pageno) {
    /* Abort Previous Ajax */
    if (loadApiListDataAjax && loadApiListDataAjax.readyState != 4) {
        loadApiListDataAjax.abort();
    }

    /* Search Keyword */
    var searchKeyword = $("#" + tableId + "SearchKeyword").val();

    /* Search Keyword length */
    var searchKeywordLen = (searchKeyword != undefined) ? searchKeyword.length : 0;

    if (searchKeywordLen == 0 || searchKeywordLen >= 3) {
        var customer_id = $('.hlb2b_deal_list').attr('data-customer-id');
        var apiName = $('#' + tableId).attr('data-apiName');
        var limit = $("#" + tableId + "Limit").val();
        var from = $('#' + tableId).attr('data-from');
        var methodName = 'get_' + from + '_front_listing_data';
        var fields = $('#' + tableId).attr('data-fields');
        fields = (fields != undefined) ? fields : '*';
        var searchFields = $('#' + tableId).attr('data-search');
        pageno = (pageno != undefined) ? pageno : 1;
        loadApiListDataAjax = $.ajax({
            url: domain_url,
            type: "post",
            dataType: "json",
            data: {
                method_name: methodName,
                api_name: apiName,
                fields: fields, /* Table field value when make listing using table*/
                shop: shop,
                limit: limit,
                pageno: pageno,
                customer_id: customer_id,
                search_keyword: searchKeyword,
                html_table_id: tableId,
                search_fields: searchFields,
                pagination_function: __loadApiListData.name
            },
            beforeSend: function () {
                var totalTH = $('#' + tableId + ' thead tr th').length;
                table_loader("table#" + tableId + " tbody", totalTH);
            },
            success: function (response) {
                if (response['code'] != undefined && response['code'] == '403') {
                    $('table#' + tableId + ' tbody').html('<tr><td colspan="7">NO record available</td></tr>');
                } else if (response['result'] == 'success') {
                    $('table#' + tableId + ' tbody').html(response['html']);
                    $('#' + tableId + 'Pagination').html(response['pagination_html']);
                } else {
                    flashNotice(response['msg']);
                }
            }
        });
    }

} /* end of __loadApiListData */
$(document).ready(function () {
    $(document).on("submit", "form[action='/cart']", function (e) {
        $('#moqError').html('');
        var ButtonId = document.activeElement.id;
        var btnHtml = $('#'+ButtonId).html();
        if (ButtonId == 'checkout') {
            e.preventDefault();
            var frmData = $(this).serialize();
            jQuery.getJSON('/cart.js', function (cart_data) {
                $.ajax({
                    url: domain_url,
                    type: "post",
                    dataType: "json",
                    data: {"method_name": 'cart_submit_draft_order', 'cart_data': cart_data, 'shop': shop},
                    beforeSend: function () {
                        loading_show('#'+ButtonId);
                    },
                    success: function (response) {
                        if (response['result'] == 'success') {
                            window.location = response['invoice_url'];
                            //$('form[action="/cart"] table').html(response['html']);
                        } else {
                            $('#moqError').html(response['msg']);
                        }
                        loading_hide_n_show_html('#'+ButtonId,btnHtml);
                    }
                });
            });
        } else {
//            e.preventDefault(); 
            return true;
        }

    });
    $(document).on("submit", ".deal_frm", function (e) {
        e.preventDefault();
        var ButtonId = document.activeElement.id;
        var btnHtml = $('#'+ButtonId).html();
        var frmData = $(this).serialize();
        frmData += '&' + $.param({"method_name": 'add_deal', 'shop': shop});
        $.ajax({
            url: domain_url,
            type: "post",
            dataType: "json",
            data: frmData,
            beforeSend: function () {
                loading_show('#'+ButtonId);
            },
            success: function (response) {
                if (response == 'select variant') {
                    $('#error_msg').html('<span style="color:red;">' + response + '</span>');
                } else if (response == 'Invalid quantity') {
                    $('#error_msg').html('<span style="color:red;">' + response + '</span>');
                } else if (response == 'Invalid price') {
                    $('#error_msg').html('<span style="color:red;">' + response + '</span>');
                } else if(response == 'Enter price less then max price'){
                    $('#error_msg').html('<span style="color:red;">' + response + '</span>');
                } else if (response == 'Enter message') {
                    $('#error_msg').html('<span style="color:red;">' + response + '</span>');
                } else if (response['code'] != '') {
                    $('#start_order').attr('onclick', '').css('background', 'green');
                    if (typeof (Shopify) == "undefined") {
                        window.location = 'inquiry.php?deal_id=' + response['code'] + '&shop=' + shop;
                    } else {
                        window.location = 'inquiry?deal_id=' + response['code'];
                    }
                }
                else {
                    window.location = domain_url + '&shop=' + shop;
//                $('#start_order span').html('Submit Inquiry').css('color', '#fff');
//                $('.success_msg').html(response['msg']);
//                $('.success_des').html(response['des']);
//                changeTab(e, 'suppiler_tab');
//                $('#suppiler_order').addClass('Polaris-Tabs__Tab--selected');
                }
                setTimeout(function () {
                    $('#error_msg').html('');
                }, 1500);
                loading_hide_n_show_html('#'+ButtonId,btnHtml);
            }
        });
    });
    if ($('.hlb2b_deal_list').length) {
        var customer_id = $('.hlb2b_deal_list').attr('data-customer-id');
        $('table[data-listing="true"]').each(function () {
            var tableId = $(this).attr('id');
            __loadApiListData(tableId);
        });
    }

    $("#variantSelect").change(function () {
        var result = $(this).find("option:selected").text();
        $(this).siblings('div').find('.Polaris-Select__SelectedOption').html(result);
    });

    $(document).on("submit", ".order_frm", function (e) {
        e.preventDefault();
        var ButtonId = document.activeElement.id;
        var btnHtml = $('#'+ButtonId).html();
        var frmData = $(this).serialize();
        frmData += '&' + $.param({"method_name": 'draft_order', 'shop': shop});
        $.ajax({
            url: domain_url,
            type: "post",
            dataType: "json",
            data: frmData,
            beforeSend: function () {
                loading_show('#'+ButtonId);
            },
            success: function (response) {
                if (response['result'] == 'success') {
                    flashNotice(response['msg']);
                    $('.sendBtn').css({display: 'none'});
                    var obj = jQuery.parseJSON(response['code']);
                    window.location = obj.invoice_url;
                } else {
                    $('#error_msg_confirm').html('<span style="color:red;">' + response['msg'] + '</span>');
                    setTimeout(function () {
                        $('#error_msg_confirm').html('');
                    }, 2000);
                }
                loading_hide_n_show_html('#'+ButtonId,btnHtml);
            }
        });
    });
    $(document).on("submit", ".buyer_deal_chat", function (e) {
        e.preventDefault();
        var frmId = '#'+$(this).attr('id');
        var ButtonId = document.activeElement.id;
        var btnHtml = $('#'+ButtonId).html();
        var frmData = $(this).serialize();
        frmData += '&' + $.param({"method_name": 'buyer_deal_chat', 'shop': shop});
        $.ajax({
            url: domain_url,
            type: "post",
            dataType: "json",
            data: frmData,
            beforeSend: function () {
                loading_show('#'+ButtonId);
            },
            success: function (response) {
                if (response['code'] != undefined && response['code'] == '403') {
                    redirect403();
                }else if(response['result'] == 'fail'){
                    $('#error_msg_confirm').html('<span style="color:red;margin-left:20px;">' + response['msg'] + '</span>')
                }else if (response['result'] == 'success') {
                    $('#show_message_negotiate').html(response['code']);
                    $("#errorMsgBlock").hide();
                    var msg_count = $('#msg_count').val();
                    if (msg_count <= 2) {
                        $('.less_more_btn').hide();
                    } else {
                        $('.less_more_btn').show();
                    }
                    location.reload();
                    $(frmId).find("input[name='buyer_message']").val('');
                    flashNotice(response['msg']);
                } else if (response['msg_content'] != undefined && response['msg_heading'] != undefined) {
                    $("#errorBlockContent").html(response['msg_content']);
                    $("#errorBlockHeading").html(response['msg_heading']);
                    $("#errorMsgBlock").show();
                    $("html, body").animate({scrollTop: 0}, "slow");
                } else {
                    flashNotice(response['msg']);
                }
                setTimeout(function () {
                    $('#errorMsg').html('');
                }, 2000);
                loading_hide_n_show_html('#'+ButtonId,btnHtml);
            }
        });
    });

    $(document).on('submit', '#buyer_message_frm', function (e) {
        e.preventDefault();
        var frmId = '#'+$(this).attr('id');
        var ButtonId = document.activeElement.id;
        var btnHtml = $('#'+ButtonId).html();
        var frmData = $(this).serialize();
        frmData += '&' + $.param({"method_name": 'buyer_deal_chat', 'shop': shop, 'wait_msg': '1'});
        $.ajax({
            url: domain_url,
            type: "post",
            dataType: "json",
            data: frmData,
            beforeSend: function () {
                loading_show('#'+ButtonId);
            },
            success: function (response) {
                if (response['code'] != undefined && response['code'] == '403') {
                    redirect403();
                }else if(response['result'] == 'fail'){
                    $('#errorMsg').html('<span style="color:red;margin-left:20px;">' + response['msg'] + '</span>')
                }  else if (response['result'] == 'success') {
                    if (response['msg'] == 'Deal-Confirmed') {
                        $('#suppiler_order').addClass('Polaris-Tabs__Tab--selected').attr('disabled', false);
                        $('#start_order').removeClass('Polaris-Tabs__Tab--selected').attr('disabled', true);
                        $('#confirme_order').removeClass('Polaris-Tabs__Tab--selected').attr('disabled', true);
                        $('#suppiler_tab').show();
                        $('#start_order_tab').hide();
                        $('#deal_place_tab').hide();
                    }
                    $('#message_content').html(response['code']);
                    $("#errorMsgBlock").hide();
                    var msg_count = $('#msg_count').val();
                    if (msg_count <= 2) {
                        $('.less_more_btn').hide();
                    } else {
                        $('.less_more_btn').show();
                    }
                    flashNotice(response['msg']);
                    $(frmId).find("input[name='buyer_message']").val('');
                } else if (response['msg_content'] != undefined && response['msg_heading'] != undefined) {
                    $("#errorBlockContent").html(response['msg_content']);
                    $("#errorBlockHeading").html(response['msg_heading']);
                    $("#errorMsgBlock").show();
                    $("html, body").animate({scrollTop: 0}, "slow");
                } else {
                    flashNotice(response['msg']);
                }
                setTimeout(function () {
                    $('#errorMsg').html('');
                }, 2000);
                loading_hide_n_show_html('#'+ButtonId,btnHtml);
            }
        });
    });

    $(".more_msg").on('click', function (e) {
        e.preventDefault();
        $(this).text(function (i, v) {
            $("#more_message").toggle();
            $('#show_message').toggle();
            $('#show_message_start').toggle();
            $('#more_message_start').toggle();
            $('#more_message_negotiate').toggle();
            $('#show_message_negotiate').toggle();
            return v === 'Less' ? 'More' : 'Less';
        });
    });
    /* ankur lakhani */
    var referral_get = getUrlParameter('referral');
    var referral_cookie = readCookie('referral');
    if (referral_get !== undefined) {
        $('#referral_tags').val(referral_get);
        createCookie('referral', referral_get, 1);
    } else {
        if (referral_cookie != null) {
            $('#referral_tags').val(referral_cookie);
        }
    }
    var product_details = new Array();
    var unit_type = '';
    var min_order_qty = '';
    if ($('.hlb2b_product_price').length) {
        var product_id = $('.hlb2b_product_price').attr('data-product-id');
        $.ajax({
            url: domain_url,
            type: "post",
            dataType: "json",
            data: {"method_name": 'get_product_price', 'product_id': product_id, 'shop': shop},
            beforeSend: function () {
            },
            success: function (response) {
                var products_details = response.products;
                unit_type = response.unit_type;
                var product_detail_price_json = JSON.parse(products_details.price_json)
                $('.hlb2b_min_order_message').html(product_detail_price_json[0].moq + ' ' + unit_type + ' (Min. Order)');

                var product_extra_options = JSON.parse(products_details.extra_options);
                var extra_options = '<div class="do-entry do-entry-separate">' +
                        '<div class="do-entry-title">Quick Details</div>' +
                        '<div class="do-entry-list">' +
                        '<dl class="do-entry-item">';
                Object.keys(product_extra_options).forEach(function (k) {
                    extra_options += '<dt class="do-entry-item">' +
                            '<span class="attr-name J-attr-name">' + product_extra_options[k].extra_title + '</span>' +
                            '</dt>' +
                            '<dd class="do-entry-item-val">' +
                            '<div class="ellipsis">' + product_extra_options[k].extra_value + '</div>' +
                            '</dd>';
                });
                extra_options += '</dl>';
                $('.extra_options').html(extra_options);
                min_order_qty = product_detail_price_json[0].moq;
                var html = '';
                Object.keys(product_detail_price_json).forEach(function (k) {
                    //console.log(k + ' - ' + product_detail_price_json[k].moq);
                    //console.log(k + ' - ' + product_detail_price_json[k].price);
                    product_details.push({'moq': product_detail_price_json[k].moq, 'price': product_detail_price_json[k].price});
                    var selected_price = '';
                    if (k == 0) {
                        selected_price = 'current-ladder-price'
                    }
                    var moq_text = product_detail_price_json[k].moq;
                    var next = parseInt(k) + 1;
                    if (product_detail_price_json[next] !== undefined) {
                        var next_moq = parseInt(product_detail_price_json[next].moq) - 1;
                        moq_text = moq_text + ' - ' + next_moq;
                    } else {
                        moq_text = '>= ' + moq_text
                    }
                    html += '<li data-role="ladder-price-item" class="ma-ladder-price-item   ladder-price-1 util-clearfix ' + selected_price + '">' +
                            '<div class="ma-quantity-range" title="10-30 Pieces">' + moq_text + ' ' + unit_type + '</div>' +
                            '<div class="ma-spec-price ma-price-promotion">' +
                            '<span class="priceVal" title="' + product_detail_price_json[k].price + '">US $' + product_detail_price_json[k].price + '</span>' +
                            '</div>' +
                            '</li>';
                });
                $('.hlb2b_product_price').html(html);
            }
        });
    }
    $('#hl-b2b-btn-order').click(function () {
        var quantity = 0;
        $('#skuWrap input').each(function () {
            //var id = parseInt($(this).attr('data-variant-id'));
            var quantity_v = parseInt($(this).attr('value'));
            quantity = quantity + quantity_v;
        });
        if (quantity < min_order_qty) {
            $('#wholesale-order-moq b').html(min_order_qty + ' ' + unit_type);
            $('#wholesale-order-moq').show();
            return false;
        }
        //var values = {};
        $('#skuWrap input').each(function () {
            var id = parseInt($(this).attr('data-variant-id'));
            var quantity = parseInt($(this).attr('value'));
            jQuery.ajax({
                type: "POST",
                url: '/cart/add.js',
                data: {'id': id, 'quantity': quantity},
                async: false
            });
        });
        window.location.href = '/cart';
    });
    $('.quantity-up').click(function () {
        var variantUp = $(this).attr('data-variant-id');
        var variantUp_qty = $('.qty-' + variantUp).val();
        var qtyUp = parseInt(variantUp_qty) + 1;
        $('.qty-' + variantUp).val(qtyUp);
        var quantity = 0;
        $('#skuWrap input').each(function () {
            //var id = parseInt($(this).attr('data-variant-id'));
            var quantity_v = parseInt($(this).attr('value'));
            quantity = quantity + quantity_v;
        });
        var m = 0;
        var final_price = '';
        if (product_details.length > 1) {
            Object.keys(product_details).forEach(function (k) {
                var startpoint = 0;
                var endpoint = 0;
                var next = parseInt(k) + 1;
                if (product_details[next] !== undefined) {
                    if (k != 0) {
                        var pre = parseInt(k) - 1;
                        startpoint = product_details[pre].moq;
                    }
                    endpoint = product_details[next].moq - 1;
                } else {
                    startpoint = parseInt(product_details[k].moq);
                    endpoint = parseInt(product_details[k].moq);
                }
                if ((quantity >= startpoint && quantity <= endpoint) && m == 0) {
                    final_price = parseFloat(product_details[k].price);
                    m++;
                }
                if (m == 0 && product_details.length == parseInt(k) + 1) {
                    final_price = parseFloat(product_details[k].price);
                    m++;
                }

            });
        } else {
            final_price = parseFloat(product_details[0].price)
        }
        final_price = final_price * quantity;
        $('.product-total-price .mark-calculator-quantity').html(quantity + ' ' + unit_type);
        $('.product-total-price .mark-calculator span').html(final_price.toFixed(2));
        if (quantity == 0) {
            $('#productCalculator').hide();
        } else {
            $('#productCalculator').show();
        }
        if (quantity >= min_order_qty) {
            $('#wholesale-order-moq').hide();
        }
    });
    $('.quantity-down').click(function () {
        var variantDown = $(this).attr('data-variant-id');
        var variantDown_qty = $('.qty-' + variantDown).val();
        var qtyDown = parseInt(variantDown_qty) - 1;
        if (qtyDown < 0) {
            qtyDown = 0;
        }
        $('.qty-' + variantDown).val(qtyDown);
        var quantity = 0;
        $('#skuWrap input').each(function () {
            //var id = parseInt($(this).attr('data-variant-id'));
            var quantity_v = parseInt($(this).attr('value'));
            quantity = quantity + quantity_v;
        });
        var m = 0;
        var final_price = '';
        if (product_details.length > 1) {
            Object.keys(product_details).forEach(function (k) {
                var startpoint = 0;
                var endpoint = 0;
                var next = parseInt(k) + 1;
                if (product_details[next] !== undefined) {
                    if (k != 0) {
                        var pre = parseInt(k) - 1;
                        startpoint = product_details[pre].moq;
                    }
                    endpoint = product_details[next].moq - 1;
                } else {
                    startpoint = parseInt(product_details[k].moq);
                    endpoint = parseInt(product_details[k].moq);
                }
                if ((quantity >= startpoint && quantity <= endpoint) && m == 0) {
                    final_price = parseFloat(product_details[k].price);
                    m++;
                }
                if (m == 0 && product_details.length == parseInt(k) + 1) {
                    final_price = parseFloat(product_details[k].price);
                    m++;
                }

            });
        } else {
            final_price = parseFloat(product_details[0].price)
        }
        final_price = final_price * quantity;
        $('.product-total-price .mark-calculator-quantity').html(quantity + ' ' + unit_type);
        $('.product-total-price .mark-calculator span').html(final_price.toFixed(2));
        if (quantity == 0) {
            $('#productCalculator').hide();
        } else {
            $('#productCalculator').show();
        }
        if (quantity >= min_order_qty) {
            $('#wholesale-order-moq').hide();
        }
    });
    $('.quantity-input').change(function () {
        var quantity = 0;
        $('#skuWrap input').each(function () {
            //var id = parseInt($(this).attr('data-variant-id'));
            var quantity_v = parseInt($(this).attr('value'));
            quantity = quantity + quantity_v;
        });
        var m = 0;
        var final_price = '';
        if (product_details.length > 1) {
            Object.keys(product_details).forEach(function (k) {
                var startpoint = 0;
                var endpoint = 0;
                var next = parseInt(k) + 1;
                if (product_details[next] !== undefined) {
                    if (k != 0) {
                        var pre = parseInt(k) - 1;
                        startpoint = product_details[pre].moq;
                    }
                    endpoint = product_details[next].moq - 1;
                } else {
                    startpoint = parseInt(product_details[k].moq);
                    endpoint = parseInt(product_details[k].moq);
                }
                if ((quantity >= startpoint && quantity <= endpoint) && m == 0) {
                    final_price = parseFloat(product_details[k].price);
                    m++;
                }
                if (m == 0 && product_details.length == parseInt(k) + 1) {
                    final_price = parseFloat(product_details[k].price);
                    m++;
                }

            });
        } else {
            final_price = parseFloat(product_details[0].price)
        }
        final_price = final_price * quantity;
        $('.product-total-price .mark-calculator-quantity').html(quantity + ' ' + unit_type);
        $('.product-total-price .mark-calculator span').html(final_price.toFixed(2));
        if (quantity == 0) {
            $('#productCalculator').hide();
        } else {
            $('#productCalculator').show();
        }
        if (quantity >= min_order_qty) {
            $('#wholesale-order-moq').hide();
        }
    });
    /* end ankur lakhani */
    /*cart page*/
    if (typeof (Shopify) != "undefined") {
        var path = __st.pageurl;
        var cartPage = path.search("/cart");
        var cart = '';
        if (cartPage > 0) {
            table_loader('.cart_view_load', 5)
            jQuery.getJSON('/cart.js', function (cart_data) {
                $.ajax({
                    url: domain_url,
                    type: "post",
                    dataType: "json",
                    data: {"method_name": 'cart_view', 'cart_data': cart_data, 'shop': shop},
                    beforeSend: function () {
                    },
                    success: function (response) {
                        if (response['result'] == 'success') {
                            $('form[action="/cart"] table').html(response['html']);
                        }
                    }
                });
            });
        }
    }


});
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function flashNotice($message, $class) {
    $class = ($class != undefined) ? $class : '';

    var flashMsgHtml = '<div class="inline-flash-wrapper animated bounceInUp inline-flash-wrapper--is-visible ourFlashMsg"><div class="inline-flash ' + $class + '  "><p class="inline-flash__message">' + $message + '</p></div></div>';

    if ($('.ourFlashMsg').length) {
        $('.ourFlashMsg').remove();
    }
    $("body").append(flashMsgHtml);

    setTimeout(function () {
        if ($('.ourFlashMsg').length) {
            $('.ourFlashMsg').remove();
        }
    }, 3000);
}

/* Ankur lakhani*/
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function deleteFromTable(tableName, primaryKeyId, pageno, tableId, what_delete, sellerName) {
    var what_delete = (what_delete == undefined) ? 'Record' : what_delete;
    var r = confirm("Are you sure you want to delete deal?");
    if (r == true) {
        $.ajax({
            url: domain_url,
            type: "post",
            dataType: "json",
            data: {method_name: "delete_from_live", shop: shop, table_name: tableName, primary_key_id: primaryKeyId},
            success: function (response) {
                if (response['result'] == 'success') {
                    location.reload();
                }
                flashNotice(response['msg']);
            }
        });
    }
}
/* CLS ADMIN DISPLAY START FORM */
  $(document).ready(function() {
            $("#give_feedback,#arr3").click(function() {
                $("#page2").show();
                $("#page3").hide();
                $("#page4").hide();
                $("#main_popup").hide();
            });

        });
        $("#ok,#arr2").click(function() {
            $("#page2").hide();
            $("#page3").show();
            $("#page4").hide();
            $("#main_popup").hide();
        });
        $("#ok2,#arr4").click(function() {
            $("#page2").hide();
            $("#page3").hide();
            $("#page4").show();
            $("#main_popup").hide();
        });
        $("#arr1").click(function() {
            $("#page2").hide();
            $("#page3").hide();
            $("#page4").hide();
            $("#main_popup").show();
        });
        
        
          $(function() {
            $('.mat-input-outer label').click(function() {
                $(this).prev('input').focus();
            });
            $('.mat-input-outer input').focusin(function() {
                $(this).next('label').addClass('active');
            });
            $('.mat-input-outer input').focusout(function() {
                if (!$(this).val()) {
                    $(this).next('label').removeClass('active');
                } else {
                    $(this).next('label').addClass('active');
                }
            });
        });
          
        new WOW().init();
         $(function() {
            $('.mat-input-outer label').click(function() {
                $(this).prev('input').focus();
            });
            $('.mat-input-outer input').focusin(function() {
                $(this).next('label').addClass('active');
            });
            $('.mat-input-outer input').focusout(function() {
                if (!$(this).val()) {
                    $(this).next('label').removeClass('active');
                } else {
                    $(this).next('label').addClass('active');
                }
            });
        });
        
        $(document).ready(function() {
            $('.rating').magicRatingInit({
                success: function(magicRatingWidget, rating) {
                    alert(rating);
                },
                iconGood: "fa-bicycle",
                iconBad: "fa-car",
            });
            $(".rating2").magicRatingInit({
                success: function(magicRatingWidget, rating) {
                    alert(rating);
                }
            })
        });
        
        
         var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36251023-1']);
        _gaq.push(['_setDomainName', 'jqueryscript.net']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
        $.fn.magicRatingInit = function(config) {

    // Init each widget return by the selector
    for (widget of $(this)) {
        var magicRatingWidget = $(widget);
        //// Get datas ////
        // Icon +
        if (magicRatingWidget.data("iconGood") == null) {
            magicRatingWidget.data("iconGood", config.iconGood != null ? config.iconGood : "fa-star");
        };

        // Icon -
        if (magicRatingWidget.data("iconBad") == null) {
            magicRatingWidget.data("iconBad", config.iconBad != null ? config.iconBad : "fa-star-o");
        };

        // Max mark
        if (magicRatingWidget.data("maxMark") == null) {
            magicRatingWidget.data("maxMark", config.maxMark != null ? config.maxMark : 5);
        }

        /*
        console.log(magicRatingWidget.data("iconGood"));
        console.log(magicRatingWidget.data("iconBad"));
        */

        // Clear the widget
        magicRatingWidget.html("");

        // Init icons
        for (i = 1; i <= magicRatingWidget.data("maxMark"); i++) {
            if (i <= magicRatingWidget.data("currentRating")) {
                magicRatingWidget.append('<i class=" ' + magicRatingWidget.data("iconGood") + ' magic-rating-icon" aria-hidden="true" data-default=true data-rating=' + i + '></i>');
            } else {
                magicRatingWidget.append('<i class=" ' + magicRatingWidget.data("iconBad") + ' magic-rating-icon" aria-hidden="true" data-default=false data-rating=' + i + '></i>');
            }
        }

        // Init reset handler
        magicRatingWidget.on("mouseleave", function() {
            var widget = $(this);
            /*
            console.log("mouseleave");
            console.log(widget.data("iconGood"));
            console.log(widget.data("iconBad"));
            */
            widget.children().each(function() {
                var icon = $(this);
                if (icon.data("default") && !icon.hasClass("fa-star")) {
                    icon.removeClass(widget.data("iconBad"));
                    icon.addClass(widget.data("iconGood"));
                } else if (!icon.data("default") && !icon.hasClass("fa-star-o")) {
                    icon.removeClass(widget.data("iconGood"));
                    icon.addClass(widget.data("iconBad"));
                }
            });
        });

        // Init click handler
        magicRatingWidget.on("click", ".magic-rating-icon", function() {
            // Get rating
            var icon = $(this);
            var widget = icon.parent();
            var rating = icon.data("rating");
            /*
            console.log("click");
            console.log(widget.data("iconGood"));
            console.log(widget.data("iconBad"));
            */
            // Update rating
            widget.children().each(function() {
                if ($(this).data("rating") <= rating) {
                    if (!$(this).hasClass(widget.data("iconGood"))) {
                        $(this).removeClass(widget.data("iconBad"));
                        $(this).addClass(widget.data("iconGood"));
                    };
                    $(this).data("default", true);
                } else {
                    if (!$(this).hasClass(widget.data("iconBad"))) {
                        $(this).removeClass(widget.data("iconGood"));
                        $(this).addClass(widget.data("iconBad"));
                    }
                    $(this).data("default", false);
                }
            });

            var callbackSuccess = config.success.bind(null, widget, rating);
            callbackSuccess();
        });

        // Init hover icons
        magicRatingWidget.on("mouseenter", ".magic-rating-icon", function() {
            var icon = $(this);
            var rating = icon.data("rating");
            var widget = icon.parent();
            /*
            console.log("mouseenter");
            console.log(widget.data("iconGood"));
            console.log(widget.data("iconBad"));
            */
            widget.children().each(function() {
                if ($(this).data("rating") <= rating) {
                    if (!$(this).hasClass(widget.data("iconGood"))) {
                        $(this).removeClass(widget.data("iconBad"));
                        $(this).addClass(widget.data("iconGood"));
                    };
                } else {
                    if (!$(this).hasClass(widget.data("iconBad"))) {
                        $(this).removeClass(widget.data("iconGood"));
                        $(this).addClass(widget.data("iconBad"));
                    }
                }
            });
        });
    }
};
