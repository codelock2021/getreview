    <form class="user member2 formdata allform">
<div class="container-fluid form1">
        <div class="container-fuild">
            <div class="text">
                <h1 class="h4 mb-4">Post-purchase emails</h1>
            </div>
            <ul class="nav nav-pills" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link active" data-toggle="pill" href="#home">Mail after purchase</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#menu1">Repeat e-mail (reminder)</a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="home" class="container-fuild tab-pane active">
                    <div class="row">
                        <div class="col-lg">
                            <form class="user">
                                <div class="tabpanel">
                                    <div class="col-xs-6">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="mail_title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">The title of the e-mail with the request for opinion</font></font></label>
                                                <input class="form-control col-md-12 col-xs-12" name="mail_title" type="text" value="Leave feedback about our store" id="mail_title">
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="mail_header"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Email header</font></font></label>
                                                <textarea class="form-control custom-campaign-input" rows="6" name="mail_header" cols="50" id="mail_header">Thank you for your trust and shopping in our store :)</textarea>
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="mail_content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">E-mail text</font></font></label>
                                                <textarea class="form-control custom-campaign-input" rows="6" name="mail_content" cols="50" id="mail_content">We hope you enjoyed the products, please feel free to share your opinion about shopping in our store.</textarea>
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="mail_submit_btn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Submit button text</font></font></label>
                                                <input class="form-control" name="mail_submit_btn" type="text" value="Submit review" id="mail_submit_btn">  
                                            </div>   
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="email_from"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Send e-mails from the address</font></font></label>
                                                <input class="form-control custom-campaign-input" data-rireq="true" data-rimail="true" name="email_from" type="email" value="" id="email_from">
                                            </div>

                                            <div class="form-group">
                                                <label for="name"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">reviews.form_name</font></font></label>
                                                <input class="form-control custom-campaign-input" name="name" type="email" id="name">
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="settings_color"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Theme color (borders, buttons, form color tags, etc.)</font></font></label>
                                                <div class="input-group colorpicker-component colorpicker-select colorpicker-element">
                                                <input class="form-control custom-campaign-input" name="settings_color" type="text" id="settings_color">
                                                    <span class="input-group-addon"><i></i></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="email_delay"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">After how many hours to send an e-mail with a request for opinion (48h by default)</font></font></label>
                                                <input class="form-control custom-campaign-input" min="1" max="1000000" name="email_delay" type="number" value="72" id="email_delay">
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="switch1">
                                                <label class="custom-control-label" for="switch1">Send reminder</label>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="form-group email_reminder_container">
                                                    <label for="email_reminder"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">After how many days to send an e-mail with a reminder (7 days by default)</font></font></label>
                                                    <input class="form-control custom-campaign-input" min="1" max="100" name="email_reminder" type="number" value="7" id="email_reminder">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div> 

                        <div class="col-lg store">
                            <div class="col-xs-6">
                                <div class="col-xs-12">
                                    <div class="form_container">
                                        <table border="0" cellpadding="0" cellspacing="0" style="background-color:#f5f5f5" width="100%">
                                            <tbody><tr>
                                                <td><table border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;border:1px solid #e3e6e8;" width="100%">
                                                <tbody><tr><td><div id="logoResult1">
                                                        <h3 style="text-align: center;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">STORE LOGO</font></font></h3>
                                                    </div></td></tr>
                                                <tr><td height="20" class="break"></td></tr>
                                                <tr>
                                                <td align="center" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;color:#252b33;font-size:34px;line-height:40px;font-weight:400;text-align:center;padding:0px 40px;font-style:normal">
                                                    <span style="color:#252b33" id="mail_header_text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thank you for your trust and shopping in our store :)</font></font></span>
                                                </td>
                                                </tr>
                                                <tr><td height="20" class="break"></td></tr>
                                                <tr>
                                                <td align="center" style="background: #fff;font-family:'Open Sans',Arial,Helvetica,sans-serif;color:#7e8890;font-size:16px;line-height:22px;font-weight:400;text-align:center;padding:0px 40px" id="" class="">
                                                    <div class="">
                                                    <p style="margin-top:0px;margin-bottom:10px" id="mail_content_text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">We hope you enjoyed the products, please feel free to share your opinion about shopping in our store.</font></font></p>
                                                    </div>
                                                </td>
                                                </tr>
                                                <tr><td height="20" class="break"></td></tr>
                                                <tr>
                                                    <td align="center" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;padding:0px 20px" class="">
                                                        <table cellpadding="0" border="0" cellspacing="0" align="center" class="">
                                                            <tbody>
                                                                <tr>
                                                                    <td height="40" align="center" class="" valign="middle">
                                                                    <a class="buttonMailBorder" id="mail_submit_btn_text" href="#" style="text-decoration: none;color: #424242 !important;border-radius: 4px;font-size: 18px;padding: 16px 30px;background-color: white;margin-bottom: 20px;margin-top: 20px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Leave a rating</font></font></a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr><td height="20" class="break"></td></tr>
                                                <tr><td height="20" class="break"></td></tr>
                                            </tbody></table></td></tr>
                                            <tr><td style="height:80px;text-align: center;" class="footer_text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Greetings and welcome back, the team of your favorite store.</font></font></td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="footer"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Address in the footer of the email</font></font></label>
                                            <textarea class="form-control custom-campaign-input" placeholder="Enter the text that will appear in the footer of the email to the customer asking for their opinion" rows="2" name="footer" cols="50" id="footer">Pozdrawiamy i zapraszamy ponownie, zespół Twojego ulubionego sklepu.</textarea>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  

                <div id="menu1" class="container-fuild tab-pane fade"><br>
                    <div class="row">
                        <div class="col-lg">
                            <div class="tabpanel">
                                <div class="col-xs-6">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="mail_title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">The title of the e-mail with the request for opinion</font></font></label>
                                            <input class="form-control col-md-12 col-xs-12" name="mail_title" type="text" value="Your opinion is important to us" id="mail_title">
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="mail_header"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reminder header</font></font></label>
                                            <textarea class="form-control custom-campaign-input" rows="6" name="mail_header" cols="50" id="mail_header">Will you find 3 minutes to show it :)?</textarea>
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="mail_content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reminder content</font></font></label>
                                            <textarea class="form-control custom-campaign-input" rows="6" name="mail_content" cols="50" id="mail_content">How do you rate recent purchases in our store?</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <div class="col-lg store">
                            <div class="col-xs-6">
                                <div class="col-xs-12">
                                    <div class="form_container">
                                        <table border="0" cellpadding="0" cellspacing="0" style="background-color:#f5f5f5" width="100%">
                                            <tbody><tr>
                                                <td><table border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;border:1px solid #e3e6e8;" width="100%">
                                                <tbody><tr><td><div id="logoResult1">
                                                        <h3 style="text-align: center;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">STORE LOGO</font></font></h3>
                                                    </div></td></tr>
                                                <tr><td height="20" class="break"></td></tr>
                                                <tr>
                                                <td align="center" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;color:#252b33;font-size:34px;line-height:40px;font-weight:400;text-align:center;padding:0px 40px;font-style:normal">
                                                    <span style="color:#252b33" id="mail_header_text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thank you for your trust and shopping in our store :)</font></font></span>
                                                </td>
                                                </tr>
                                                <tr><td height="20" class="break"></td></tr>
                                                <tr>
                                                <td align="center" style="background: #fff;font-family:'Open Sans',Arial,Helvetica,sans-serif;color:#7e8890;font-size:16px;line-height:22px;font-weight:400;text-align:center;padding:0px 40px" id="" class="">
                                                    <div class="">
                                                    <p style="margin-top:0px;margin-bottom:10px" id="mail_content_text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">We hope you enjoyed the products, please feel free to share your opinion about shopping in our store.</font></font></p>
                                                    </div>
                                                </td>
                                                </tr>
                                                <tr><td height="20" class="break"></td></tr>
                                                <tr>
                                                    <td align="center" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;padding:0px 20px" class="">
                                                        <table cellpadding="0" border="0" cellspacing="0" align="center" class="">
                                                            <tbody>
                                                                <tr>
                                                                    <td height="40" align="center" class="" valign="middle">
                                                                    <a class="buttonMailBorder" id="mail_submit_btn_text" href="#" style="text-decoration: none;color: #424242 !important;border-radius: 4px;font-size: 18px;padding: 16px 30px;background-color: white;margin-bottom: 20px;margin-top: 20px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Leave a rating</font></font></a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr><td height="20" class="break"></td></tr>
                                                <tr><td height="20" class="break"></td></tr>
                                            </tbody></table></td></tr>
                                            <tr><td style="height:80px;text-align: center;" class="footer_text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Greetings and welcome back, the team of your favorite store.</font></font></td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="footer"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Address in the footer of the email</font></font></label>
                                            <textarea class="form-control custom-campaign-input" placeholder="Enter the text that will appear in the footer of the email to the customer asking for their opinion" rows="2" name="footer" cols="50" id="footer">Pozdrawiamy i zapraszamy ponownie, zespół Twojego ulubionego sklepu.</textarea>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <button class="step btn btn-success btn-lg btn-flat1 step-next1" id="nextBtn" data-direction="next" type="button"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">next</font></font></button>
        </div>  
</div>
    </form>