<?php include 'cls_header.php'; ?>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard.php">
    <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Getreview</div>
</a>
<hr class="sidebar-divider my-0">
<li class="nav-item active">
    <a class="nav-link" href="">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>global.dashboard</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="">
        <i class="fas fa-envelope"></i>
        <span>global.edit_mail_request</span>
    </a>
</li>
<li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <i class="fas fa-users"></i>
                    <span>global.moderations</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="buttons.html">General moderation</a>
                        <a class="collapse-item" href="cards.html">Product moderation</a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-credit-card"></i>
                    <span>global.payments</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="utilities-color.html">global.buy_plan</a>
                        <a class="collapse-item" href="utilities-border.html">global.shop_list</a>
                        <a class="collapse-item" href="utilities-animation.html">global.proforma</a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Tools</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="">Gerwazy</a>
                        <a class="collapse-item" href="">Multiple links</a>
                        <a class="collapse-item" href="">Import</a>
                        <a class="collapse-item" href="">Google Rich Snippet</a>
                        <a class="collapse-item" href="">Minimodule</a>
                        <a class="collapse-item" href="">Black list</a>
                        <a class="collapse-item" href="">Social Proof</a>
                        <a class="collapse-item" href="">Remarketing</a> 
                        <a class="collapse-item" href="">Newsletter</a>
                        <a class="collapse-item" href="">Facebook Page Tab</a>
                        <a class="collapse-item" href="">Video review</a>
                        <a class="collapse-item" href="">TrustBox</a>
                        <a class="collapse-item" href="">competition</a>
                        <a class="collapse-item" href="">SEO</a>
                    </div>
                </div>
            </li>
<li class="nav-item">
    <a class="nav-link" href="">
        <i class="fas fa-question-circle"></i>
        <span>global.faq</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="">
        <i class="fas fa-cog"></i>
        <span>Settings</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="">
        <i class="fas fa-book"></i>
        <span>global.tutorial</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="">
        <i class="fas fa-fw fa-table"></i>
        <span>Tables</span></a>
</li>
</ul>
