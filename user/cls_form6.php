<form class="user member6 formdata allform">
    <div class="container-fluid form1">
        <div class="row">
            <div class="text">
                <h1 class="h4 mb-4">Company's data</h1>
            </div>
            <div class="col-lg">
                <div class="tabpanel">
                    <div class="col-xs-6">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="mail_title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Company's data</font></font></label>
                                <input class="form-control col-md-12 col-xs-12" name="mail_title" type="text" value="" id="mail_title">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="mail_submit_btn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tax Number</font></font></label>
                                <input class="form-control" name="mail_submit_btn" type="text" value="Submit review" id="mail_submit_btn">  
                            </div>   
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="email_from"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">E-mail</font></font></label>
                                <input class="form-control custom-campaign-input" data-rireq="true" data-rimail="true" name="email_from" type="email" value="" id="email_from">
                            </div>

                            <div class="form-group">
                                <label for="name"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Street</font></font></label>
                                <input class="form-control custom-campaign-input" name="name" type="email" id="name">
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="settings_color"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Postcode</font></font></label>
                                <input class="form-control custom-campaign-input" name="settings_color" type="text" id="settings_color">
                            </div>

                            <div class="form-group">
                                <label for="email_delay"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">City</font></font></label>
                                <input class="form-control custom-campaign-input" min="1" max="1000000" name="email_delay" type="number" value="" id="email_delay">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="settings_color"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phone number</font></font></label>
                                <input class="form-control custom-campaign-input" name="settings_color" type="text" id="settings_color">
                            </div>

                            <div class="form-group">
                                <label for="email_delay"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Country</font></font></label>
                                <input class="form-control custom-campaign-input" min="1" max="1000000" name="email_delay" type="number" value="" id="email_delay">
                            </div>
                            <div>
                                <button class="step btn btn-success btn-lg btn-flat1 step-next1" id="nextBtn" data-direction="next" type="button"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">global.next</font></font></button>
                            </div>   
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>
</form>

