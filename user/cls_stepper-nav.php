<div class="stepper-nav">
    <button class="step btn btn-success btn-lg btn-flat step-prev" data-direction="prev" type="button"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Back</font></font></button>
    <button class="step btn btn-success btn-lg btn-flat step-next" data-direction="next" type="button"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">global.next</font></font></button>
  
  
    <h3 class="step-title" data-hide-on-step="2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Create a new feedback widget</font></font></h3>
  
    <ul class="stepper-steps">
        <li class="steps">
                    <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot ">1</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">URL address
                    </font></font>
                </span>
        </li>

        <li class="steps">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">2</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Emails after purchase
                    </font></font>
                </span>
        </li>

        <li class="steps">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">3</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Widget on the site
                    </font></font>
                </span>
        </li>

        <li class="steps">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">4</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Questions for the client
                    </font></font>
                </span>
        </li>

        <li class="steps">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">5</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Moderation settings
                    </font></font>
                </span>
        </li>

        <li class="steps">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">6</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">statute
                    </font></font>
                </span>
        </li>
<!--        <li class="steps">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">7</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;"> Reward for opinions 
                    </font></font>
                </span>
            </a>
        </li>-->
    </ul>
  
</div>