<?php  
require_once('include.php');
include_once('cls_header.php');
?>
<body class="bg-gradient-primary">
    <div class="container">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block">
                       <img  src="<?php echo CLS_SITE_URL; ?>/assets/images/time_for_review_shutterstock.jpg" alt="your image" width="480" height="602"/> 
                    </div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                            </div>
                            <form class="user"   method="post" id="register" name="register_frm" >
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                         <span class="message first_name"></span>
                                        <input type="text" class="form-control form-control-user" name="first_name" id="exampleFirstName" placeholder="First Name" >
                                    </div>
                                    <div class="col-sm-6">
                                         <span class="message last_name"></span>
                                        <input type="text" class="form-control form-control-user" name="last_name" id="exampleLastName"  placeholder="Last Name" >
                                        
                                    </div>
                                </div>
                                    <div class="col-sm-12">
                                <div class="form-group">
                                     <span class="message email"></span>
                                    <input type="email" class="form-control form-control-user" name="email" id="exampleInputEmail"  placeholder="Email Address" >
                                </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                         <span class="message password"></span>
                                        <input type="password" class="form-control form-control-user" name="password"  id="exampleInputPassword" placeholder="Password" >
                                    </div>
                                    <div class="col-sm-6">
                                         <span class="message confirm_password"></span>
                                        <input type="password" class="form-control form-control-user" name="confirm_password" id="exampleRepeatPassword" placeholder="Repeat Password" >
                                    </div>
                                </div>
                                <button type="submit"  id="addregister" name="submit" class="btn btn-primary btn-user btn-block" >Register Account</button>
                                <hr>
                                <a href="index.php" class="btn btn-google btn-user btn-block">
                                    <i class="fab fa-google fa-fw"></i> Register with Google
                                </a>
                                <a href="index.php" class="btn btn-facebook btn-user btn-block">
                                    <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                                </a>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="forgot-password.php">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="login.php">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>