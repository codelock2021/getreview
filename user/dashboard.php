<?php
ob_start();
include_once('cls_header.php');
include_once('../append/session.php');
$common_function = new common_function();
?>
<!DOCTYPE html>
<?php include 'cls_header.php'; ?>
<body id="page-top">
<div id="wrapper">
  <?php include 'cls_sidebar.php'; ?>
         <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <?php include 'cls_topbar.php'; ?>
                <div class="container-fluid">
                    <div class="row">
                    </div>
                    <?php include 'cls_stepper-nav.php'; ?>
                    <?php include 'cls_login.php'; ?>
                    <?php include 'cls_email.php'; ?>
                    <?php include 'cls_widget.php'; ?>
                    <?php include 'cls_form4.php'; ?>
                    <?php include 'cls_form5.php'; ?>
                    <?php include 'cls_form6.php'; ?>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2021</span>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
$(document).ready(function() {
$(".dot").click(function(){
  $('.active').removeClass('active');
    $(this).addClass('active');    
    var selected_number = $(this).text();
     $(".allform").each(function(){
     		$(this).hide();
     });
     setTimeout(function(){
     $('.member'+selected_number).show();
     },2000);
     
    });
   
});
</script>

</html>
