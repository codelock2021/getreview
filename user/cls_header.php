<?php
include_once '../append/connection.php';
include_once ABS_PATH . '/user/cls_functions.php';
include_once ABS_PATH . '/cls_shopifyapps/config.php';

//$default_shop = 'managedashboard.myshopify.com';
//
//if ((isset($_GET['store']) && $_GET['store'] != '') || isset($default_shop)) {
//   $store = isset($_GET['store']) ? $_GET['store'] : $default_shop;
//    $functions = new Client_functions($store);
//    $ologin = new Login();
//    $current_user = $functions->get_store_detail_obj();
//    $login_user = $functions->get_login_user_data();
//    
//     if (!empty($login_user)) {
//         $current_user = array_merge((array) $current_user, $login_user);
//     } else {
//         header('Location: ' . SITE_CLIENT_URL . '?store=' . $store);
//         exit;
//     }
//
//    if (empty($current_user)) {
//         header('Location: https://www.shopify.com/admin/apps');
//         exit;
//    }
//} else {
//    header('Location: https://www.shopify.com/admin/apps');
//    exit;
//}
//$view = (isset($_GET["view"]) && $_GET["view"]) ? $_GET["view"] : FALSE;
?>  
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo CLS_SITE_NAME; ?></title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"    rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/sb-admin-2.css'); ?>" />
        <link rel="stylesheet" href="<?php echo main_url('assets/css/style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo main_url('assets/css/whatsapp.css'); ?>" />
        <link rel="stylesheet" href="<?php echo main_url('assets/vendor/datatables/dataTables.bootstrap4.min.css'); ?>" />
        <link rel="stylesheet" href="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.css">
        <script src="<?php echo main_url('assets/js/jquery-2.1.1.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/jquery-ui.min.js'); ?>"></script>
        <script  src="<?php echo main_url('assets/js/sb-admin-2.js'); ?>" /></script>
        <script type="text/javascript" src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.js"></script>
        <script src="<?php echo main_url('assets/js/shopify_client.js'); ?>"></script>
        <script src="<?php echo main_url('assets/vendor/datatables/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php echo main_url('assets/vendor/datatables/dataTables.bootstrap4.min.js'); ?>"></script>
        <script  src="<?php echo main_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>" /></script>
        <script  src="<?php echo main_url('assets/vendor/jquery-easing/jquery.easing.min.js'); ?>" /></script>

    <link href="../assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="../assets/css/index.css" rel="stylesheet"/>
    <script src="../assets/vendor/jquery/jquery.min.js"></script>
    <script src="./../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="./../assets/js/sb-admin-2.min.js"></script>