    <form class="user member4 formdata allform">
<div class="container-fluid form1">
        <div class="row">
            <div class="text">
                <h1 class="h4 mb-4">Questions for the client</h1>
            </div>

           <div class="col-sm-12 col-xl-10 col-xl-offset-1">
              <div><a class="btn btn-success" id="starsCreate"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Editing questions</font></font></a></div>

              <div id="stars_edit_hints" style="" class="alert alert-info"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Remember that if you want to transfer data to Google Rich Snippet, you must select "product" for at least one of the questions to customers</font></font></div>
            </div>
        </div>
</div>
    </form>