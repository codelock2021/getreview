<form class="user member3 formdata allform">
    <div class="container-fluid form1">
        <div class="row">
            <div class="text">
                <h1 class="h4 mb-4">Main settings</h1>
            </div>

            <div class="col-xs-12">
                <div class="form-group1">
                    <label for="sel1">Select a store engine</label>
                    <select class="form-control" id="sel1" oninput="this.className = ''">
                        <option>Select a store engine</option>
                        <option>Shoper</option>
                        <option>PrestaShop</option>
                        <option>Woocommerce</option>
                        <option>Esklep</option>
                        <option>Another-Baselinker</option>
                        <option>Other</option>
                    </select>
                </div>    
            </div>
        </div>
    </div>

    <div class="container-fluid sub-form1">
        <div class="row">
            <div class="text">
                <h1 class="h4 mb-4">Display</h1>
            </div>

            <div class="col-xs-12 w1">
                <div class="form-group1">
                    <label for="email_delay"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Number of displayed items in desktop mode</font></font></label>
                    <input class="form-control custom-campaign-input" min="1" max="1000000" name="email_delay" type="number" value="10" id="email_delay">
                </div>
            </div>

            <div class="col-xs-12 w1">
                <div class="form-group1">
                    <label for="email_delay"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Number of displayed items in desktop mode</font></font></label>
                    <input class="form-control custom-campaign-input" min="1" max="1000000" name="email_delay" type="number" value="6" id="email_delay">
                </div>
            </div>

            <div class="col-xs-12 w1">
                <div class="c-form-group1">
                    <label for="sel1">Choose sort type</label>
                    <select class="form-control" id="sel1">
                        <option>View all opinions everywhere</option>
                        <option>Product only opinion</option>
                        <option>Order: Product, Category, All</option>
                        <option>Choose closest type: Product or Category or all opinions.</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12 w1">
                <div class="c-form-group1">
                    <label for="sel1">Choose sort Order</label>
                    <select class="form-control" id="sel1">
                        <option>Newest</option>
                        <option>Oldest</option>
                    </select>
                </div>
            </div>


            <div class="widget">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="switch5">
                    <label class="custom-control-label" for="switch5">reviews.sort_photo_first</label>
                </div>

                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="switch2">
                    <label class="custom-control-label" for="switch2">Prioritize feedback from video</label>
                </div>

                <p>In the case of two prioritization, the video will be Before After.</p>
            </div>

            <div class="widget1">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="switch3">
                    <label class="custom-control-label" for="switch3">reviews.view_on_all_pages</label>
                </div>

                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="switch4">
                    <label class="custom-control-label" for="switch4">reviews.view_on_products_pages</label>
                </div>
            </div>

            <div class="col-xs-12 w2">
                <div class="form-group2">
                    <label for="mail_content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">reviews.view_on</font></font></label>
                    <textarea class="form-control custom-campaign-input" rows="6" name="mail_content" cols="50" id="mail_content"></textarea>
                </div>
            </div>

            <div class="col-xs-12 w3">
                <div class="form-group2">
                    <label for="mail_content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">reviews.view_not</font></font></label>
                    <textarea class="form-control custom-campaign-input" rows="6" name="mail_content" cols="50" id="mail_content"></textarea>
                </div>
            </div>

            <div class="col-xs-12 w2">
                <div class="form-group2">
                    <label for="mail_content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">reviews.basket_urls</font></font></label>
                    <textarea class="form-control custom-campaign-input" rows="6" name="mail_content" cols="50" id="mail_content"></textarea>
                </div>
            </div>

            <div class="col-xs-12 w3">
                <div class="form-group2">
                    <label for="mail_content"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">reviews.final_urls</font></font></label>
                    <textarea class="form-control custom-campaign-input" rows="6" name="mail_content" cols="50" id="mail_content"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid sub-form1">
        <div class="row">
            <div class="text">
                <h1 class="h4 mb-4">Personalization</h1>
            </div>

            <div class="col-xs-12 w1">
                <div class="text">
                    <div class="text">
                        <h6 class="p-text">Rating_icons</h6>
                    </div>
                    <div class="icon">
                        <p>filled icon:
                        <div class="form-group">
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>   
                        </p> 
                    </div>

                    <div class="icon">
                        <p>unfilled icon:
                        <div class="form-group">
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>   
                        </p> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid form1">
        <div class="row">
            <div class="text">
                <h1 class="h4 mb-4">Preview of the feedback widget on the website</h1>
            </div></br>
        </div>

        <div class="container">
            <div class="row">
                <div class="text">
                    <h5 class="p-text">Average store rating</h5>
                    <div class="box">
                        <div class="box-t">
                            <h6>4.3</h6>
                        </div>
                        <div class="box-p">
                            <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Below we present the opinions of our clients</font></font></p>
                        </div>

                        <div class="ratting">
                            <div class="r1">
                                <i class="fas fa-star"><span>5</span></i>
                                <div class="progress" style="height:10px">
                                    <div class="progress-bar" style="width:40%;"></div>
                                </div>
                                <span class="s1">(3)</span>
                            </div> 

                            <div class="r1">
                                <i class="fas fa-star"><span>4</span></i>
                                <div class="progress" style="height:10px">
                                    <div class="progress-bar" style="width:33.333333333333%;"></div>
                                </div>
                                <span class="s1">(2)</span>
                            </div>    

                            <div class="r1">
                                <i class="fas fa-star"><span>3</span></i>
                                <div class="progress" style="height:10px">
                                    <div class="progress-bar" style="width:16.666666666667%;"></div>
                                </div>
                                <span class="s1">(1)</span>
                            </div>   

                            <div class="r1">
                                <i class="fas fa-star"><span>2</span></i>
                                <div class="progress" style="height:10px">
                                    <div class="progress-bar"></div>
                                </div>
                                <span class="s1">(0)</span>
                            </div>    

                            <div class="r1">
                                <i class="fas fa-star"><span>1</span></i>
                                <div class="progress" style="height:10px">
                                    <div class="progress-bar"></div>
                                </div>
                                <span class="s1">(0)</span>
                            </div>    
                        </div>   
                    </div> 
                </div>
            </div>

            <div class="row c1">
                <div class="col-xl-3 col-md-6">
                    <div class="card ">
                        <div class="card-body">
                            <div class="text-xs font-weight-bold text-primary text-uppercase">
                                Hive</br>Added: 2018-12-10</div>
                            <div class="image">
                                <img src="../assets/img/96129.jpg" class="img1">
                            </div>
                        </div>

                        <div class="rat">
                            <p>Store rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </p>

                            <p>Rating of products:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Delivery Rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Delivery Rating:</p>
                            <p class="pt">Shoes as in the picture but size a little non-standard, 38 should be more 37.5</p>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card ">
                        <div class="card-body">
                            <div class="text-xs font-weight-bold text-primary text-uppercase">
                                Tom</br>Added: 2018-12-15</div>
                            <div class="image">
                                <img src="../assets/img/29951.jpeg" class="img2">
                            </div>
                        </div>

                        <div class="rat1">
                            <p>Store rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </p>

                            <p>Rating of products:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Delivery Rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Delivery Rating:</p>
                            <p class="pt">Very nice quality shoes, the same size as in the offer. The waiting time for the parcel to be corrected.</p>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card ">
                        <div class="card-body">
                            <div class="text-xs font-weight-bold text-primary text-uppercase">
                                Amanda</br>Added: 2019-01-04</div>
                            <div class="image">
                                <img src="../assets/img/65683.jpeg" class="img3">
                            </div>
                        </div>

                        <div class="rat">
                            <p>Store rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </p>

                            <p>Rating of products:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Delivery Rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Delivery Rating:</p>
                            <p class="pt">Great quality goods, I am very pleased! :): D Unfortunately, the courier was in no hurry :(</p>
                        </div>
                    </div>
                </div>
            </div>  

            <div class="row c1">
                <div class="col-xl-3 col-md-6">
                    <div class="card ">
                        <div class="card-body">
                            <div class="text-xs font-weight-bold text-primary text-uppercase">
                                Michalina</br>Added: 2018-12-15</div>
                        </div>

                        <div class="rat">
                            <p>Store rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Rating of products:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </p>

                            <p>Delivery Rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Delivery Rating:</p>
                            <p class="pt">Great boots, happy spouse :)</p>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card ">
                        <div class="card-body">
                            <div class="text-xs font-weight-bold text-primary text-uppercase">
                                Tom</br>Added: 2018-12-16</div>
                        </div>

                        <div class="rat1">
                            <p>Store rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </p>

                            <p>Rating of products:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </p>

                            <p>Delivery Rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </p>

                            <p>Delivery Rating:</p>
                            <p class="pt">All OK.</p>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card ">
                        <div class="card-body">
                            <div class="text-xs font-weight-bold text-primary text-uppercase">
                                Paul</br>Added: 2019-01-07</div>
                        </div>

                        <div class="rat">
                            <p>Store rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Rating of products:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Delivery Rating:
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </p>

                            <p>Delivery Rating:</p>
                            <p class="pt">Good quality slippers, a bit of fur sheds hair, but they warm the feet pleasantly :) Overall, I recommend it.</p>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</form>