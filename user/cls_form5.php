            <form class="user member5 formdata allform">
<div class="container-fluid form1">
    <div class="row">
        <div class="row">
            <div class="text">
                <h1 class="h4 mb-4">Moderation settings</h1>
            </div>
        </div>
        <div class="col-lg">
            <div class="radio"> 
                <div class="radiobtn">
                    <label>Global lock</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="switch8">
                        <label class="custom-control-label" for="switch8"> Moderated reviews (each new opinion must be approved by the administrator before being displayed on the website).</label>
                    </div>
                    <tr>
                </div>
                 <div class="radiobtn">
                    <label>Optional locks</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="switch9">
                        <label class="custom-control-label" for="switch9"> Automatically block posts with an average rating below:</label>
                    </div>
                    <div class="form-group">
                                <input class="form-control custom-campaign-input" name="email_delay" id="email_delay">
                    </div>
                    <tr>
                </div>
                <div class="radiobtn">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="switch10">
                        <label class="custom-control-label" for="switch10">Automatically block entries if one of the ratings is below:</label>
                    </div>
                     <div class="form-group">
                                <input class="form-control custom-campaign-input" name="email_delay" id="email_delay">
                     </div>
                    <tr>
                    <label>Notifications</label>
                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                        <label class="form-check-label" for="flexRadioDefault1">
                          Default radio
                        </label>
                      </div>
                     <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                        <label class="form-check-label" for="flexRadioDefault2">
                          Default radio
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
                        <label class="form-check-label" for="flexRadioDefault2">
                          Default checked radio
                        </label>
                      </div>
                         <div>
            <button class="step btn btn-success btn-lg btn-flat1 step-next1" id="nextBtn" data-direction="next" type="button"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">global.next</font></font></button>
        </div>   
         </div>
        </div> 
        </div>
    </div>
</div>
</form> 

