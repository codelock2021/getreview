    <form class="user member1 allform">
<div class="container-fuild form">
    <div class="text">
        <h1 class="h4 mb-4">Page URL</h1>
    </div>
        <div class="col-sm-12 col-lg-10 col-lg-offset-1">
            <div class="form-group">
                <label for="domain">
                <font style="vertical-align: inherit;">
                <font style="vertical-align: inherit;">Enter the URL of your store's website (The address should also contain https: // if you have an SSL certificate or http: // if the store does not have a certificate)</font></font></label>
                <input class="form-control custom-campaign-input f1" data-rireq="false" data-riurl="false" name="domain" type="text" id="regForm">
            </div>
        </div>
        <div class="form-group">
            <label for="sel1">Select a store engine</label>
            <select class="form-control f2" id="sel1">
                <option>Select a store engine</option>
                <option>Shoper</option>
                <option>PrestaShop</option>
                <option>Woocommerce</option>
                <option>Esklep</option>
                <option>Another-Baselinker</option>
                <option>Other</option>
            </select>
        </div>
        <div class="form-group">
            <label for="sel1">Choose an integration method</label>
            <select class="form-control f2"id="sel1">
                <option>Choose an integration method</option>
            </select>
        </div>
        <div>
            <button class="step btn btn-success btn-lg btn-flat1 step-next1" id="nextBtn" data-direction="next" type="button"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">global.next</font></font></button>
        </div>  
</div> 
    </form>

