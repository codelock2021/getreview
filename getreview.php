<?php
      
header("Access-Control-Allow-Origin: *");
include_once ('append/connection.php');
include_once ('append/forground-functions.php');

ob_clean();
$is_bad_shop = 0;
$comeback = array('result' => 'fail', 'msg' => 'Opps! Bad request call!');
if (isset($_POST['routine_name']) && $_POST['routine_name'] != '' && isset($_POST['store']) && $_POST['store'] != '') {
    $functions = new forground_functions($_POST['store']);
    $comeback = call_user_func(array($functions, $_POST['routine_name']));
    echo json_encode($comeback);;
    exit;
} else {
    $is_bad_shop++;
}

if ($is_bad_shop > 0) {
    echo json_encode($comeback);
    exit;
}
