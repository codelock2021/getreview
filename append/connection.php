<?php
error_reporting(E_ALL);
ini_set("error_reporting", E_ALL);
 session_start();
if ($_SERVER['SERVER_NAME'] == 'localhost') {
    define("DB_SERVER", "localhost");
    define("DB_DATABASE", "getreview");
    define("DB_USERNAME", "root");
    define("DB_PASSWORD", "");
    define("DB_OBJECT", "mysql"); 
    define('MODE', 'dev');
    define('ABS_PATH', dirname(dirname(__FILE__)));
    define('MAIN_URL', 'http://localhost/getreview');
    define('CLS_SITE_URL', 'http://localhost/getreview');
    define('SITE_CLIENT_URL', 'http://localhost/getreview/user/');
    define('CLS_TRACK_PATH', $_SERVER['DOCUMENT_ROOT']);
    define('SITE_ADMIN_URL', 'http://localhost/getreview/admin/');
} elseif ($_SERVER['SERVER_NAME'] == 'codelocksolutions.com') {
    define("DB_SERVER", "193.178.43.3");
    define("DB_DATABASE", "opicon");
    define("DB_USERNAME", "refericon");
    define("DB_PASSWORD", "l8P8DkQ.5CWtS1AQ");
    define("DB_OBJECT", "mysql");
    define('MODE', 'live');
    define('ABS_PATH', dirname(dirname(__FILE__)));
    define('MAIN_URL', 'https://codelocksolutions.com/getreview');
    define('CLS_SITE_URL', 'https://codelocksolutions.com/getreview');
    define('CLS_TRACK_PATH', $_SERVER['DOCUMENT_ROOT'].'/getreview/logs/');
    define('SITE_CLIENT_URL', 'https://codelocksolutions.com/getreview/user/');
    define('SITE_ADMIN_URL', 'https://codelocksolutions.com/getreview/admin/');
} else {
    echo 'Undefine host';
    exit;
}
if (!function_exists('main_url')) {
    function main_url($uri = '') {
        $main_url = '';
        if (MAIN_URL == '') {
            echo "<pre>";
            print_r("Please set MAIN_URL");
            echo "</pre>";
            exit;
        } elseif (MAIN_URL != '' && $uri != '') {
            return MAIN_URL . '/' . $uri;
        } elseif (MAIN_URL != '') {
            return MAIN_URL . '/';
        }
    }

}
$lang = 'english';
require_once ABS_PATH . '/language/' . $lang . '/common_constant_msg.php';
class DB_Class {
    var $db; 
    function __construct() {
        $this->db=mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
   
        try {
            if (DB_OBJECT == "mysql") {
                $odbclink = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_DATABASE, DB_USERNAME, DB_PASSWORD);
                $odbclink->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $odbclink->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);               
                if (true === empty($odbclink)) {
                    echo "No mysql connection";
                    exit;
                }
            } else {
                if (MODE == "dev") {
                    $m = new MongoClient(DB_SERVER);
                    $odbclink = $m->selectDB(DB_DATABASE);
                } else {
                }
                if (true === empty($odbclink)) {    
                    echo "No mongodb connection";
                    exit;
                }
            }
            $GLOBALS['conn'] = $this->db;
            return $this->db;
        } catch (Exception $error) {
            echo $error->getMessage();
            exit;
        }
        }
                
}
define('CLS_SITE_NAME','getreview');
define('CLS_SITE_EMAIL', '#');
define('CLS_NO_IMAGE','no-image.png');
define('CHARGE', '0.99');
define('CLS_APP_shop_URL', '#');
define('CLS_SITE_COPYRIGHT', CLS_SITE_NAME . ' &copy; ' . date('Y') . ' - Made with team');   
define('CLS_SVG_EYE', '<i class="fas fa-eye" style="font-size: 25px;color:#5e5e5e"></i>');
define('CLS_SVG_CLOSE_EYE', '<i class="fas fa-eye-slash" style="font-size: 25px;color:#5e5e5e;"></i>');
define('CLS_SVG_EDIT', '<i class="fas fa-edit" style="font-size: 25px; color:#5e5e5e;"></i>');
define('CLS_SVG_DELETE', '<i class="fas fa-trash-alt" style="font-size: 25px; color:#5e5e5e;"></i>');
define('CLS_SVG_NEXT_PAGE', '<i class="fas fa-arrow-right" style="font-size: 25px;color:#5e5e5e;"></i>');
define('CLS_SVG_PREV_PAGE', '<i class="fas fa-arrow-left" style="font-size: 25px;color:#5e5e5e;"></i>');
define('CLS_SVG_SEARCH', '<i class="fas fa-search" style="font-size: 25px;color:#5e5e5e;"></i>');
define('CLS_CLS_SVG_RESET', '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path style="fill:#030104;" d="M18,10.473c0-1.948-0.618-3.397-2.066-4.844c-0.391-0.39-1.023-0.39-1.414,0c-0.391,0.391-0.391,1.024,0,1.415C15.599,8.122,16,9.051,16,10.473c0,1.469-0.572,2.85-1.611,3.888c-1.004,1.003-2.078,1.502-3.428,1.593l1.246-1.247c0.391-0.391,0.391-1.023,0-1.414s-1.023-0.391-1.414,0L7.086,17l3.707,3.707C10.988,20.902,11.244,21,11.5,21s0.512-0.098,0.707-0.293c0.391-0.391,0.391-1.023,0-1.414l-1.337-1.336c1.923-0.082,3.542-0.792,4.933-2.181C17.22,14.36,18,12.477,18,10.473z"/><path style="fill:#030104;" d="M5,10.5c0-1.469,0.572-2.85,1.611-3.889c1.009-1.009,2.092-1.508,3.457-1.594L8.793,6.292c-0.391,0.391-0.391,1.023,0,1.414C8.988,7.902,9.244,8,9.5,8s0.512-0.098,0.707-0.293L13.914,4l-3.707-3.707c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414l1.311,1.311C8.19,3.104,6.579,3.814,5.197,5.197C3.78,6.613,3,8.496,3,10.5c0,1.948,0.618,3.397,2.066,4.844c0.195,0.195,0.451,0.292,0.707,0.292s0.512-0.098,0.707-0.293c0.391-0.391,0.391-1.024,0-1.415C5.401,12.851,5,11.922,5,10.5z"></path></svg>');
define('CLS_SVG_CIRCLE_PLUS', '<svg class="Polaris-Icon__Svg" viewBox="0 0 510 510" focusable="false" aria-hidden="true"><path d="M255,0C114.75,0,0,114.75,0,255s114.75,255,255,255s255-114.75,255-255S395.25,0,255,0z M382.5,280.5h-102v102h-51v-102    h-102v-51h102v-102h51v102h102V280.5z" fill-rule="evenodd" fill="#3f4eae"></path></svg>');
define('CLS_SVG_CIRCLE_MINUS', '<svg class="Polaris-Icon__Svg" viewBox="0 0 80 80" focusable="false" aria-hidden="true"><path d="M39.769,0C17.8,0,0,17.8,0,39.768c0,21.956,17.8,39.768,39.769,39.768   c21.965,0,39.768-17.812,39.768-39.768C79.536,17.8,61.733,0,39.769,0z M13.261,45.07V34.466h53.014V45.07H13.261z" fill-rule="evenodd" fill="#DE3618"></path></svg>');
define('CLS_TABLE_LOGIN_USER', 'login_user');
define('CLS_TABLE_SHOP_USERS', 'user_shops');
define('CLS_TABLE_OPICON_USER', 'users');

define("CLS_API_VERSIION",'api/2021-01');
define('DATE', date('Y-m-d H:i:s'));
define('CLS_PAGE_PER', '5');
define("ENTITY_WENT_INCORRECT_report", "Something went incorrect");
define("BACKDROP_GENERATED_SUCCESS_report", "Report updated successfully");
define("PRODUCT_WORD_COLOR_REQUIRED_report", "The Product subject text color required");
define("PRODUCT_WORD_COLOR_FORMAT_report", "The Product subject text color should have used");
define("PRODUCT_FONT_AREA_NEEDED_report", "The Product subject font size needed");
define("PRODUCT_FONT_AREA_ONLYFLOAT_report", "The Product subject font size must be float used");
define("REAL_TOTAL_COLOR_NEEDED_report", "Real total color Needed");
define("REAL_TOTAL_COLOR_PATTERN_report", "Real total color should have hex used");
define("REAL_TOTAL_FONT_SIZE_NEEDED_report", "Real total font size Needed");
define("REAL_TOTAL_FONT_SIZE_ONLYFLOAT_report", "Real total font size must be float used");
define("MARKDOWN_TOTAL_COLOR_NEEDED_report", "Markdown total color Needed");
define("MARKDOWN_TOTAL_COLOR_FORMAT_report", "Markdown total color should have hex used");
define("MARKDOWN_TOTAL_FONT_SIZE_NEEDED_report", "Markdown total font size Needed");
define("MARKDOWN_TOTAL_FONT_SIZE_ONLYFLOAT_report", "Markdown total font size must be float used");
define("PROFILE_WIDTH_NEEDED_report", "Profile width needed");
define("PROFILE_WIDTH_ONLYFLOAT_report", "Profile width must be float used");
define("PROFILE_HEIGHT_REQUIRED_report", "Profile height Needed");
define("PROFILE_HEIGHT_ONLYFLOAT_report", "Profile height must be float used");
define("NOT_ALL_MESSAGE_NEEDED_report", "Not all of above message required");
define("NOT_ALL_COLOR_NEEDED_report", "Not all above message color needed");
define("NOT_ALL_COLOR_PARRERN_report", "Not all above message color should have hex used");
define("NOT_ALL_SIZE_NEEDED_report", "Not all above message font size needed");
define("NOT_ALL_FONT_SIZE_ONLYFLOAT_report", "Not all above message font size must be float used");
define("CLS_POST_NOT_SUBMIT_ERROR_report", "Post  are not used submiT");
define("SENDING_TYPE_IMPORVE_SUCCESS_report", 'Sending type updated successfully.');
define("SENDING_TYPE_GENERATE_SUCCESS_report", 'Sending type generated successfully.');
define("SENDING_TYPE_RECORD_FETCHED_SUCCESS_report", 'Sending type data fetched successfully.');
define("SENDING_TYPE_PLAN_EXPIRE_report", 'Opps ! Your plan is expired :).');
define("SENDING_TYPE_PLAN_NOT_SUBSCRIBE_report", 'Opps ! You have not subcription :).');
define("SENDING_TYPE_RANK_CHANGE_SUCCESS_report", 'Rank changed successfully !');
define("SENDING_TYPE_REMOVED_SUCCESS_report", 'Remove successfully !');
define("SENDING_APP_RANK_CHANGE_SUCCESS_report", 'App Remove changed successfully !');
define("CLS_SOMETHING_WENT_WRONG", "Something went wrong");


if (!isset($__variousLanguageNeeded) || (isset($__variousLanguageNeeded) && $__variousLanguageNeeded === true)) {
    require_once ABS_PATH . '/collection/pomo_library/pomo/mo.php';
    require_once ABS_PATH . '/collection/pomo_library/l10n.php';
    require_once ABS_PATH . '/collection/pomo_library/formatting.php';
    require_once ABS_PATH . '/collection/pomo_library/calender_lang_locale.php'; 
    $database_class = new DB_Class();
    $connection = $GLOBALS['conn'];
    $store_name = (isset($_GET['store']) && $_GET['store'] != '') ? $_GET['store'] : null;
    $user_lang = 'en';

    if (!isset($store_name)) {
        $store_name = (isset($_POST['store']) && $_POST['store'] != '') ? $_POST['store'] : null;
    }
/*
//    if (isset($store_name)) {
//       
//        $query = $connection->prepare("SELECT application_language FROM `user_shops` WHERE store_name = '$store_name' LIMIT 1;");
//        $query->execute();
//        $query->setFetchMode(PDO::FETCH_OBJ);
//        $cls_rows = $query->fetch();
//        $user_lang = $cls_rows->application_language;
//    }
*/
    $locations = $locations = array();
    $locations = array($user_lang);
    $locations[] = ABS_PATH . '/language';

    foreach ($locations as $locale) {
        foreach ($locations as $location) {
            if (file_exists($location . '/' . $locale . '.mo')) {
                load_textdomain('default', $location . '/' . $locale . '.mo');
            }
        }
    }
    $calder_locale_obj = new Calender_Locale();
}
function  generate_log($inventory = 'General', $log_information = 'test') {
    
    if (MODE == 'live') {
        $log_filled_track = CLS_TRACK_PATH . $inventory . '/' . date('Y-m-d') . ".txt";
        $directoryname = dirname($log_filled_track);
   

        if (!is_dir($directoryname)) {
            mkdir($directoryname, 0777, true);
        }
        
        $cls_myfile = fopen($log_filled_track, "a+") or die("file is not generated");

        $str = "\n\n" . '---------------------' . date('H:i:s') . "\n" . $log_information . "\n" . '-*-*-*-*-*-*-*-*-*';
        fwrite($cls_myfile, $str);
        fclose($cls_myfile);
    } else {
        echo $str = str_replace('\n', '<br>', "\n\n" . '---------------------' . date('H:i:s') . "\n" . $log_information . "\n" . '-*-*-*-*-*-*-*-*-*');
    }
}

