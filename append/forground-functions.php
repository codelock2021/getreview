<?php

include ABS_PATH . "/cls_shopifyapps/config.php";
if (DB_OBJECT == 'mysql') {
    include ABS_PATH . "/collection/mongo_mysql/mysql/common_function.php";
} else {
    include ABS_PATH . "/collection/mongo_mysql/mongo/common_function.php";
}

class Forground_functions extends common_function {

    /**
     * @var array collection of error messages
     */
    public $errors = array();

    /**
     * @var array collection of success / neutral messages
     */
    public $messages = array();

    /**
     * the function "__construct()" automatically starts whenever an object of this class is created,
     * you know, when you do "$login = new Login();"
     */
    public function __construct($shop = '') {
        /* call parent's (common_function) constructor */
        parent::__construct($shop);
    }

    function prepare_db_inputs($post) {
        $post_value = mysqli_real_escape_string($this->db_connection, trim($post));
        return $post_value;
    }

    /**
     * Get shop informaion by Shop name
     */
    public function get_shop($shop) {
        $where = 'WHERE store_name = "' . $shop . '"';
        $response = $this->select_result(TABLE_CLIENT_STORES, '*', $where);
        return $response;
    }

    private function get_table_listing_query_front_data($table_id, $limit, $offset, $search_word = NULL, $customer_id = NULL) {
        $current_user = $this->get_login_user_data();
        $select_array = array();
        $seller_cond_str = '';
        $select_array_customer = array();
        if (isset($select_seller) && $select_seller != '') {
            $select_array = array('seller_id' => $select_seller);
            $select_array_customer = array('c.seller_id' => $select_seller);
            $seller_cond_str = " AND FIND_IN_SET('$select_seller',seller_ids) > 0";
        }

        $query_arr = array(
            'dealsDataList' => array(
                'name' => TABLE_DEAL,
                'fields' => '*',
                'search_fields' => 'customer_name|product_title',
                'where' => array("status" => '1', 'customer_id' => $customer_id),
                'order_by' => NULL,
                'group_by' => NULL,
                'join_arr' => array(),
            )
        );

        $retrun_arr = array();

        if (isset($query_arr[$table_id])) {
            $T = (object) $query_arr[$table_id];
            $retrun_arr['api_name'] = $T->name;
            if ($table_id == 'orderData') {
                $T->where = str_replace('CURRENT_USERID', $current_user['user_id'], $T->where);
            }

            $retrun_arr['filtered_count'] = $retrun_arr['total_prod_cnt'] = $this->get_total_record($T->name, $T->where, $T->group_by, $T->join_arr);

            if (isset($search_word) && $search_word != '') {
                $T->where = $this->make_table_search_query($search_word, $T->search_fields, $T->where);

                /* count wit search query (filter) result */
                $retrun_arr['filtered_count'] = $this->get_total_record($T->name, $T->where, $T->group_by, $T->join_arr);
            }
            $retrun_arr['data_arr'] = $this->get_record_with_join($T->name, $T->fields, $T->where, $T->order_by, $T->group_by, $limit, $offset, $T->join_arr);
            $retrun_arr['query'] = $this->get_record_with_join($T->name, $T->fields, $T->where, $T->order_by, $T->group_by, $limit, $offset, $T->join_arr);
        }
        return $retrun_arr;
    }

    function get_table_front_listing_data() {
        $response = array('result' => 'fail', 'msg' => __('Something went wrong'));
        if (isset($_POST['shop']) && $_POST['shop'] != '') {
            $shopinfo = $this->current_store_obj;

            /* for limit */
            $per_page = defined('PAGE_PER') ? PAGE_PER : 10;
            $limit = isset($_POST['limit']) ? $_POST['limit'] : $per_page;

            /* for current page */
            $pageno = isset($_POST['pageno']) ? $_POST['pageno'] : '1';
            $offset = $limit * ($pageno - 1);
            $search_word = (isset($_POST['search_keyword']) && $_POST['search_keyword'] != '') ? $_POST['search_keyword'] : NULL;

            $customer_id = (isset($_POST['customer_id']) && $_POST['customer_id'] != '') ? $_POST['customer_id'] : NULL;

            $get_table_arr = $this->get_table_listing_query_front_data($_POST['html_table_id'], $limit, $offset, $search_word, $customer_id);

            $filtered_count = $get_table_arr['filtered_count'];
            $total_prod_cnt = $get_table_arr['total_prod_cnt'];
            $table_data_arr = $get_table_arr['data_arr'];

            /* make row (tr) for each record fetch from api */
            /* below line called the function make_data_{api_name} */
            $tr_html = call_user_func(array($this, 'make_table_data_front_' . $_POST['html_table_id']), $table_data_arr, $pageno, $get_table_arr['api_name']);

            /* for pagination */
            $total_page = ceil($filtered_count / $limit);
            $pagination_html = $this->pagination_btn_html($total_page, $pageno, $_POST['pagination_function'], $_POST['html_table_id']);
            /* for pagination */

            /* prepare success response */
            $response = array(
                "result" => 'success',
                "recordsTotal" => intval($total_prod_cnt),
                "recordsFiltered" => intval($filtered_count),
                'pagination_html' => $pagination_html,
                'html' => $tr_html
            );
            return $response;
        }
    }

    function pagination_btn_html($total_page, $current_page, $pagination_function, $table_id) {
        $pagination_html = $is_next_btn_disabled = $is_prev_btn_disabled = '';
        if ($total_page > 1) {
            /* Is Prev Btn Disabled */
            $is_prev_btn_disabled = ($current_page == 1 || $current_page > $total_page) ? 'Polaris-Button--disabled' : '';

            /* Is Next Btn Disabled */
            $is_next_btn_disabled = ($total_page == $current_page) ? 'Polaris-Button--disabled' : '';

            $pagination_html = '
            <div class="Polaris-Page__Pagination">
                <nav class="Polaris-Pagination Polaris-Pagination--plain" aria-label="Pagination">
                    <div id="" class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                        <div class="Polaris-ButtonGroup__Item">
                            <a href="javascript:void(0)" onclick="' . $pagination_function . '(\'' . $table_id . '\',' . ($current_page - 1) . ')" class="Polaris-Button tip display_inline_block ' . $is_prev_btn_disabled . '" data-hover="Previous">
                                <span class="Polaris-Button__Content">
                                    <span>
                                        <span class="Polaris-Icon">
                                        ' . SVG_PREV_PAGE . '
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                        <div class="Polaris-ButtonGroup__Item">
                            <a href="javascript:void(0)" onclick="' . $pagination_function . '(\'' . $table_id . '\',' . ($current_page + 1) . ')" class="Polaris-Button display_inline_block tip ' . $is_next_btn_disabled . '" data-hover="Next">
                                <span class="Polaris-Button__Content">
                                    <span>
                                        <span class="Polaris-Icon">
                                        ' . SVG_NEXT_PAGE . '
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                </nav>
            </div>';
        }

        return $pagination_html;
    }

  

    function draft_order() {
        /* Get data from table */
        $response = array('result' => 'fail', 'msg' => SOMETHING_WENT_WRONG_MSG);
        if (isset($_POST['deal_id']) && $_POST['deal_id'] != '' && $_POST['deal_id'] > 0) {

            $where = array('id' => $_POST['deal_id']);
            $deal_data_obj = $this->select_row(TABLE_DEAL, 'final_price', $where);
            $final_price_db = $deal_data_obj->final_price;
            if ($_POST['buyer_price'] == '' || $_POST['buyer_price'] < 1) {
                $response['msg'] = 'Invalid Price';
            } elseif ($_POST['buyer_price'] != $_POST['seller_price']) {
                $response['msg'] = 'Buyer price and seller price not match';
            } elseif ($_POST['seller_price'] != $final_price_db) {
                $response['msg'] = 'You should enter actual price which is discussed';
            } else {
                $is_comfirmed = 0;
                $line_items = array();
                $variant_id = $this->prepare_db_inputs($_POST['variant_id']);
                $requested_qty = trim($_POST['requested_qty']);
                $original_price = trim($_POST['original_price']);
                $value = $original_price - $final_price_db;
                $amount = $value * $requested_qty;
                $line_items[] = array(
                    "variant_id" => $variant_id,
                    "quantity" => $requested_qty,
                    "applied_discount" => array(
                        "title" => "Negotiate",
                        "description" => "",
                        "value" => $value,
                        "value_type" => "fixed_amount",
                        "amount" => $amount
                    )
                );

                $final_price = trim($_POST['buyer_price']);
                $seller_price = trim($_POST['seller_price']);
                if (isset($_POST['confirm_by_buyer'])) {
                    $is_comfirmed = $_POST['confirm_by_buyer'];
                }
                $fields = array(
                    'is_confirmed' => $is_comfirmed,
                    'requested_price' => $final_price,
                    'final_price' => $seller_price,
                    'is_new_message' => '1',
                );
                $where = array('id' => $_POST['deal_id']);
                $order_confirmed = $this->update(TABLE_DEAL, $fields, $where);
                if ($_POST['buyer_message'] != '') {
                    $msg_fields = array('sender' => 'buyer', 'message' => $this->prepare_db_inputs($_POST['buyer_message']), 'deal_id' => $_POST['deal_id']);
                    $msg = $this->insert(TABLE_CHATTING, $msg_fields);
                }
                if (!empty($line_items)) {
                    $draft_order_array = array("draft_order" =>
                        array(
                            "line_items" => $line_items,
                            "note_attributes" => array(array('name' => 'deal', 'value' => $_POST['deal_id'])),
                        )
                    );
                    if (isset($post->customer_id) && $post->customer_id > 0) {
                        $draft_order_array['draft_order']['customer'] = array('id' => $_POST['customer_id']);
                    }
                    $draft_order = shopify_call($this->current_store_obj->token, $this->current_store_obj->store_name, "/admin/draft_orders.json", json_encode($draft_order_array), 'POST', array("Content-Type: application/json"));
                    $draft_order = json_decode($draft_order['response']);

                    $invoice_url = json_encode(array('invoice_url' => $draft_order->draft_order->invoice_url));

                    if ($draft_order) {
                        $response = array('result' => 'success', 'msg' => 'success', 'code' => $invoice_url);
                    }
                }
            }
        }
        return $response;
    }

    /**/

    public function get_product_price() {
        $product_id = $_POST['product_id'];
        $product_details = $this->select_row(TABLE_PRODUCTS, '*', array('product_id' => $product_id));
        $unit_type_details = $this->select_row(TABLE_UNIT_TYPE, '*', array('id' => $product_details->unit_type_id));
        //$variant_details = $this->select_result(TABLE_VARIANTS, '*', array('p_id' => $product_details->id));
        echo json_encode(array('products' => $product_details, 'unit_type' => $unit_type_details->name, 'help_option' => $product_details->help_option));
        exit;
    }

    private function order_validation() {
        $extra_msg = array();
        $validation_rule_array = array(
            array('field' => 'buyer_price', 'label' => 'Buyer-price', 'rules' => 'required'),
            array('field' => 'seller_price', 'label' => 'Seller-price', 'rules' => 'required'),
        );
        if ($_POST['buyer_price'] != $_POST['seller_price']) {
            $extra_msg = array('msg' => 'Buyer price not match with seller price');
        }
        return $this->validation('Product', $validation_rule_array, $extra_msg);
    }

  

    public function is_json($args) {
        json_decode($args);
        return (json_last_error() === JSON_ERROR_NONE);
    }

    function make_moq_range($price_rule_arr = array(), $money_format = '') {
        $range_value_arr = array();
        if (!empty($price_rule_arr)) {

            foreach ($price_rule_arr as $key => $value) {
                $next_key = $key + 1;
                if (isset($price_rule_arr[$next_key]['moq'])) {
                    /* not last value */
                    $range_value_arr[$key]['range'] = $value['moq'] . ' - ' . ($price_rule_arr[$next_key]['moq'] - 1) . ' : ' . $this->money_formate_replace($money_format, $value['price']);
                    $range_value_arr[$key]['start'] = $value['moq'];
                    $range_value_arr[$key]['end'] = ($price_rule_arr[$next_key]['moq'] - 1);
                } else {
                    $range_value_arr[$key]['range'] = ' >= ' . $value['moq'] . ' : ' . $this->money_formate_replace($money_format, $value['price']);
                    $range_value_arr[$key]['start'] = $value['moq'];
                    $range_value_arr[$key]['end'] = $value['moq'];
                    /* last value */
                }
                $range_value_arr[$key]['price'] = $value['price'];
            }
        }
        return $range_value_arr;
    }

    function count_qty_cart_product($cart_items_arr) {
        $product_qty_arr = array();
        foreach ($cart_items_arr as $item) {
            $product_id = $item['product_id'];
            if (isset($product_qty_arr[$product_id])) {
                $product_qty_arr[$product_id] = $product_qty_arr[$product_id] + trim($item['quantity']);
            } else {
                $product_qty_arr[$product_id] = trim($item['quantity']);
            }
        }
        return $product_qty_arr;
    }

    function product_price_qty_wise($product_rule_arr, $product_qty_arr, &$err_arr) {
        $product_price_arr = array();
        foreach ($product_qty_arr as $c_product_id => $qty) {
            $product_rule = $product_rule_arr[$c_product_id];
            $price = NULL;
            $max_price = NULL;
            $moq = NULL;
            foreach ($product_rule as $range) {
                if (!isset($moq)) {
                    $moq = $range['start'];
                }
                if (!isset($max_price)) {
                    $max_price = $range['price'];
                }
                if ($qty >= $range['start'] && $qty <= $range['end'] || ($qty >= $range['start'] == $qty <= $range['end']) && $qty >= $range['end']) {
                    $price = $range['price'];
                }
            }
            if (isset($price)) {
                $product_price_arr[$c_product_id] = $price;
            } else {
                $err_arr[$c_product_id] = "You should buy min $moq quantity of ";
                $product_price_arr[$c_product_id] = $max_price;
            }
        }
        return $product_price_arr;
    }

    function cart_view() {
        $response = array('result' => 'fail', 'msg' => SOMETHING_WENT_WRONG_MSG);
        if (isset($_POST['cart_data']) && is_array($_POST['cart_data']) && !empty($_POST['cart_data'])) {
            $current_user = $this->get_store_detail_obj();
            $money_format = $current_user->money_format;

            /* Unit type array */
            $where = array('status' => '1');
            $unit_type_data = $this->select_result(TABLE_UNIT_TYPE, 'id,name', $where);
            $unit_type_data = json_decode(json_encode($unit_type_data), TRUE);
            $unit_type_data = array_column($unit_type_data, 'name', 'id');

            $cart_data = $_POST['cart_data'];
            if (isset($cart_data['items']) && is_array($cart_data['items']) && !empty($cart_data['items'])) {
                $cart_items_arr = $cart_data['items'];
                $product_id_arr = array_column($cart_items_arr, 'product_id');
                $uni_product_id_arr = array_unique($product_id_arr);
                $product_ids = implode(',', $uni_product_id_arr);
                $select_prod_qry = "SELECT * FROM " . TABLE_PRODUCTS . " WHERE product_id IN ($product_ids)";
                $prod_qry_reso = $this->query($select_prod_qry);

                $product_rule_arr = array();
                $product_detail = array();
                if ($prod_qry_reso->num_rows > 0) {
                    while ($product = $prod_qry_reso->fetch_object()) {
                        $product_detail[$product->product_id] = $product;
                        if ($product->price_json != '' && $this->is_json($product->price_json)) {
                            $price_rule_arr = json_decode(($product->price_json), TRUE);
                            $product_rule_arr[$product->product_id] = $this->make_moq_range($price_rule_arr, $money_format);
                        } else {
                            $product_rule_arr[$product->product_id] = array();
                        }
                    }
                }

                /* get product wise Quantity */
                $product_qty_arr = $this->count_qty_cart_product($cart_items_arr);

                /* get our price of product */
                $err_arr = array();
                $product_price_arr = $this->product_price_qty_wise($product_rule_arr, $product_qty_arr, $err_arr);
                $html = '<thead>
                            <tr>
                                <th class="image" style="width:200px">&nbsp;</th>
                                <th class="item">Product</th>
                                <th class="qty">Qty</th>
                                <th class="">Unit</th>
                                <th class="">Unit_price</th>
                                <th class="price">Price</th>
                                <th class="remove">Remove</th>
                            </tr>
                        </thead>
                        <tbody class="cart_view_load">';
                $cart_total = 0.00;
                foreach ($cart_items_arr as $key => $item) {
                    $cart_product_id = $item['product_id'];
                    $unit_price_html = '';
                    foreach ($product_rule_arr[$cart_product_id] as $range) {
                        if ($range['price'] == $product_price_arr[$cart_product_id]) {
                            $color_red = '';
                            if (isset($err_arr[$cart_product_id])) {
                                $color_red = 'style="color:red"';
                            }
                            $range['range'] = '<strong ' . $color_red . '>' . $range['range'] . '</strong>';
                        }
                        $unit_price_html .= '<li>' . $range['range'] . '</li>';
                    }

                    if ($unit_price_html != '') {
                        $unit_price_html = '<ul>' . $unit_price_html . '</ul>';
                    }

                    $line = $key + 1;
                    $unit_id = $product_detail[$cart_product_id]->unit_type_id;
                    $unit_type = isset($unit_type_data[$unit_id]) ? $unit_type_data[$unit_id] : '-';
                    //<img src="&#10;&#10;&#10;&#10;&#10;&#10;&#10;&#10;&#10;&#10;&#10;&#10;&#10;' . $item['image'] . '&#10;&#10;&#10;&#10;" alt="' . $item['product_title'] . '">
                    $cart_total += ($product_price_arr[$cart_product_id] * $item['quantity']);
                    $html .= '
                    <tr>
                        <td class="image" style="" data-line="' . $line . '">
                            <div class="product_image">
                                <a href="' . $item['url'] . '">
                                <img src="' . $item['image'] . '" alt="' . $item['product_title'] . '">
                                </a>
                            </div>
                        </td>
                        <td class="item">
                            <a href="' . $item['url'] . '">
                            <strong>' . $item['product_title'] . '</strong>
                            <span class="variant_title">' . $item['variant_title'] . '</span>
                            </a>
                            <br>
                        </td>
                        <td class="qty">
                            <input type="text" size="4" name="updates[]" id="updates_' . $item['variant_id'] . '" value="' . $item['quantity'] . '" onfocus="this.select();" class="tc item-quantity">
                        </td>
                        <td>' . $unit_type . '</td> 
                        <td>' . $unit_price_html . '</td>
                        <td class="price">' . $this->money_formate_replace($money_format, ($product_price_arr[$cart_product_id] * $item['quantity'])) . '</td>
                        <td class="remove"><a href="/cart/change?line=' . $line . '&amp;quantity=0" class="cart "><i class="lnr lnr-cross"></i></a></td>
                    </tr>';
                }
                $html .= '
                    <tr class="summary">
                        <td colspan="5">Total price : </td>
                        <td class="price"><span class="total"><strong>' . $this->money_formate_replace($money_format, $cart_total) . '</strong></span></td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>';
                $response['result'] = 'success';
                $response['msg'] = 'success';
                $response['html'] = $html;
            }/* End of valid cart item condition */
        }/* End of valid cart condition */
        return $response;
    }

    function cart_submit_draft_order() {
        $response = array('result' => 'fail', 'msg' => SOMETHING_WENT_WRONG_MSG);
        if (isset($_POST['cart_data']) && is_array($_POST['cart_data']) && !empty($_POST['cart_data'])) {
            $current_user = $this->get_store_detail_obj();
            $money_format = $current_user->money_format;

            $cart_data = $_POST['cart_data'];
            if (isset($cart_data['items']) && is_array($cart_data['items']) && !empty($cart_data['items'])) {
                $cart_items_arr = $cart_data['items'];
                $product_id_arr = array_column($cart_items_arr, 'product_id');
                $uni_product_id_arr = array_unique($product_id_arr);
                $product_ids = implode(',', $uni_product_id_arr);
                $select_prod_qry = "SELECT * FROM " . TABLE_PRODUCTS . " WHERE product_id IN ($product_ids)";
                $prod_qry_reso = $this->query($select_prod_qry);

                $product_rule_arr = array();
                $product_detail = array();
                if ($prod_qry_reso->num_rows > 0) {
                    while ($product = $prod_qry_reso->fetch_object()) {
                        $product_detail[$product->product_id] = $product;
                        if ($product->price_json != '' && $this->is_json($product->price_json)) {
                            $price_rule_arr = json_decode(($product->price_json), TRUE);
                            $product_rule_arr[$product->product_id] = $this->make_moq_range($price_rule_arr, $money_format);
                        } else {
                            $product_rule_arr[$product->product_id] = array();
                        }
                    }
                }

                /* get product wise Quantity */
                $product_qty_arr = $this->count_qty_cart_product($cart_items_arr);

                /* get our price of product */
                $err_arr = array();
                $product_price_arr = $this->product_price_qty_wise($product_rule_arr, $product_qty_arr, $err_arr);

                if (empty($err_arr)) {
                    $cart_total = 0.00;
                    $line_items = array();
                    foreach ($cart_items_arr as $key => $item) {
                        $cart_product_id = $item['product_id'];
                        $original_price = $product_detail[$cart_product_id]->price;
                        $final_price_db = $product_price_arr[$cart_product_id];
                        $value = $original_price - $final_price_db;
                        $amount = $value * $item['quantity'];
                        $line_items[] = array(
                            "variant_id" => $item['variant_id'],
                            "quantity" => $item['quantity'],
                            "applied_discount" => array(
                                "title" => "Negotiate",
                                "description" => "",
                                "value" => $value,
                                "value_type" => "fixed_amount",
                                "amount" => $amount
                            )
                        );
                    }

                    if (!empty($line_items)) {
                        $draft_order_array = array("draft_order" =>
                            array(
                                "line_items" => $line_items
                            )
                        );

                        /* if (isset($post->customer_id) && $post->customer_id > 0) {
                          $draft_order_array['draft_order']['customer'] = array('id' => $_POST['customer_id']);
                          } */

                        $draft_order = shopify_call($this->current_store_obj->token, $this->current_store_obj->store_name, "/admin/draft_orders.json", json_encode($draft_order_array), 'POST', array("Content-Type: application/json"));
                        $draft_order = json_decode($draft_order['response']);

                        if ($draft_order) {
                            $response = array('result' => 'success', 'msg' => 'success', 'invoice_url' => $draft_order->draft_order->invoice_url);
                        }
                    }
                } else {
                    $err_html = '';
                    /* qty error handling */
                    foreach ($err_arr as $prod_id => $err_msg) {
                        $err_html .= '<li>' . $err_msg . '<strong>' . $product_detail[$prod_id]->product_title . '</strong> product for complete your order</li>';
                    }
                    $response['msg'] = $err_html;
                }
            }/* End of valid cart item condition */
        }/* End of valid cart condition */
        return $response;
    }
    
    function prepare_html(){
       $reviewHtml = '<div class="cls-rev-widg cls--js" data-number-of-reviews="0" data-average-rating="0.00">
            <div class="cls-rev-widg__header">
                <h2 class="cls-rev-widg__title">Customer Reviews</h2>
                <div class="cls-rev-widg__summary">
                    <div class="cls-rev-widg__summary-stars"><a class="cls-star cls--off"></a><a class="cls-star cls--off"></a><a class="cls-star cls--off"></a><a class="cls-star cls--off"></a><a class="cls-star cls--off"></a></div>
                </div>
                <!--                <div class="cls-widget-actions-wrapper">
                                    <a href="#" class="cls-write-rev-link close-form-btn" aria-expanded="true">Cancel review</a></div>-->
                <div class="cls-rev__br"></div>
                <div class="cls-form-wrapper" style="display: block;">
                    <form class="cls-form" novalidate="novalidate">
                        <div class="cls-form__-fieldset">
                            <label for="cls_review_reviewer_name_0eawnvum">Name</label>
                            <input id="cls_review_reviewer_name_0eawnvum" name="reviewer_name" type="text" placeholder="Enter your name (public)" aria-label="Name" class="cls_style_box"> 
                        </div>
                        <div class="cls-form__-fieldset">
                            <label for="cls_review_reviewer_email_0eawnvum">Email</label>
                            <input id="cls_review_reviewer_email_0eawnvum" name="reviewer_email" type="email" class="cls_style_box" required="" placeholder="Enter your email (private)" aria-label="Email" aria-required="true">
                        </div>

                        <div class="cls-form__-fieldset">
                            <label for="cls_review_reviewer_email_0eawnvum">Rating</label>
                        </div>

                        <div class="rate">
                            <input type="radio" id="star5" name="rate" value="5" />
                            <label for="star5" title="text">5 stars</label>
                            <input type="radio" id="star4" name="rate" value="4" />
                            <label for="star4" title="text">4 stars</label>
                            <input type="radio" id="star3" name="rate" value="3" />
                            <label for="star3" title="text">3 stars</label>
                            <input type="radio" id="star2" name="rate" value="2" />
                            <label for="star2" title="text">2 stars</label>
                            <input type="radio" id="star1" name="rate" value="1" />
                            <label for="star1" title="text">1 star</label>
                        </div>
                        </br>
                        </br>
                        <div class="cls-form__-fieldset">
                            <label for="cls_review_reviewer_name_0eawnvum">Review Title</label><span class="cls-countdown"></span>
                            <input id="cls_review_title_0eawnvum" name="review_title" type="text" placeholder="Give your review a title" aria-label="Review Title" class="cls_style_box">
                        </div>
                        <div class="cls-form__-fieldset">
                            <label for="cls_review_body_0eawnvum">Review</label><span class="cls-countdown"></span>
                        </div>
                        <div class="cls-form__-fieldset">
                            <textarea id="cls_review_bodytext_0eawnvum" rows="5" name="review_body" placeholder="Write your comments here" aria-label="Review" class="cls_style_textbox"></textarea>
                        </div>
                        <div class="cls-custom-forms">
                          <button type="submit" name="submit" class="cls-submit-rev btn btn_c button ">Submit Review </button>
                          </div>
                    </form>
                </div>
            </div>';

    $response['result'] = 'success';
    $response['msg'] = 'success';
    $response['html'] = $reviewHtml;
    return $response;
    }

}
